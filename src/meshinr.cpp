
// Setup program arguments
#include <boost/program_options.hpp>
namespace po = boost::program_options;

#define DEBUG_TRACE
#define CGAL_MESH_3_VERBOSE
//#define CGAL_SURFACE_MESHER_VERBOSE


// Inexact CGAL Kernel (fixed precision (double) numeric types)
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>


// Read Inr Format
#include <CGAL/Image_3.h>


// Mesh domains from implicit functions
#include <CGAL/Implicit_mesh_domain_3.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/Labeled_mesh_domain_3.h>


// Reconstruct smooth surfaces (and mesh domains) from rough data input
#include "make_smooth_polyhedron_from_discrete_image.h"
#include <CGAL/Mesh_polyhedron_3.h>
#include <CGAL/Side_of_triangle_mesh.h>


// Generate a 2d/3d mesh from a mesh domain
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Surface_mesh_default_triangulation_3.h>

#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Surface_mesh_complex_2_in_triangulation_3.h>

#include <CGAL/Mesh_constant_domain_field_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Surface_mesh_default_criteria_3.h>

#include <CGAL/make_mesh_3.h>
#include <CGAL/make_surface_mesh.h>


// Output meshes to disk
#include <CGAL/IO/Complex_2_in_triangulation_3_file_writer.h>
#include "output_polyhedron_to_vtu.h"
#include "output_polyhedron_to_stl.h"
#include "output_c3t3_to_dolfin_xml.h"
#include "output_c3t3_to_patran.h"


// Compute minimum bounding spheres
#include "compute_bounding_sphere.h"


#include <algorithm>
#include <fstream>
#include <exception>
#include <cstring>
#include <cerrno>



///////////////////////////////////////////////////////////////////////////////
// Implicit Mesh Function Wrappers                                           //
///////////////////////////////////////////////////////////////////////////////
//
// Define wrappers for mixed callable types in function lists
//


// Abstract interface for callable-like functions /////////////////////////////
//
// We'll use this to allow other function types to be wrapped, stored
// and called using this interface.
// This is necessary because we ultimately want to use a mix of domain
// functions defined by polyhedra and kernel primitives, and maybe others.
//
template<typename Kernel>
class FunctionBase {
public:
    typedef typename Kernel::FT FT;
    typedef typename Kernel::Point_3 Point_3;
    typedef typename Kernel::Sphere_3 Sphere_3;

    virtual FT operator()(const Point_3& point) const = 0;

    virtual Sphere_3 bounding_sphere() const = 0;
};


// Callable implicit function wrapper /////////////////////////////////////////
//
// This should wrap any implicit function type.
// I'm not sure about the construction/destruction scope when using
// the "const T& f" construction though :/
//
template<typename T, typename Kernel>
class Function: public FunctionBase<Kernel> {
public:
    typedef FunctionBase<Kernel>   Base;
    typedef typename Base::FT      FT;
    typedef typename Base::Point_3 Point_3;
    typedef typename Base::Sphere_3 Sphere_3;

    Function(const T& function, const Sphere_3& bounding_sphere = Sphere_3()):
        function(function),
        bounding_sphere_(bounding_sphere)
    {}

    FT operator()(const Point_3& point) const {
        return function(point);
    }

    Sphere_3 bounding_sphere() const {
        return bounding_sphere_;
    }

    const T& function;
    Sphere_3 bounding_sphere_;
};


// Kernel primitive wrapper ///////////////////////////////////////////////////
//
// Kernel functions don't provide an implicit function interface,
// instead providing a set of predicates we can query.
// These are wrapped here into an implicit function domain.
//
template<
    typename T, typename Kernel = typename CGAL::Kernel_traits<T>::Kernel
>
class KernelFunction: public FunctionBase<Kernel> {
public:
    typedef FunctionBase<Kernel>   Base;
    typedef typename Base::FT      FT;
    typedef typename Base::Point_3 Point_3;
    typedef typename Base::Sphere_3 Sphere_3;

    KernelFunction(
        const T& function, const Sphere_3& bounding_sphere = Sphere_3()
    ):
        function(function),
        bounding_sphere_(bounding_sphere)
    {}

    FT operator()(const Point_3& point) const {
        //
        // Of the options available, this seemed the
        // least confusing function to use and interpret for
        // a closed volume.
        //
        if(function.has_on_bounded_side(point)) {
            return -1;
        } else {
            return 1;
        }
    }

    Sphere_3 bounding_sphere() const {
        return bounding_sphere_;
    }

    const T& function;
    Sphere_3 bounding_sphere_;
};


// Polyhedron wrapper /////////////////////////////////////////////////////////
//
// Polyhedra don't actually provide a callable type, but the
// Side_of_triangle_mesh function should provide something much like
// the kernel has_on_bounded_side methods.
//
template<
    typename Polyhedron,
    typename Kernel = typename Polyhedron::Traits
>
class PolyhedronFunction: public FunctionBase<Kernel> {
public:
    typedef FunctionBase<Kernel>   Base;
    typedef typename Base::FT      FT;
    typedef typename Base::Point_3 Point_3;
    typedef typename Base::Sphere_3 Sphere_3;

    CGAL::Side_of_triangle_mesh<Polyhedron, Kernel> in_polyhedron;
    

    PolyhedronFunction(const Polyhedron& p):
        in_polyhedron(p),
        bounding_sphere_(compute_bounding_sphere<Kernel>(
            p.points_begin(), p.points_end()
        ))
    {}

    FT operator()(const Point_3& point) const {
        // Test if point is on unbounded side of the polyhedron.
        // If it's on the unbounded side, it's outside, so return 1.
        // If it's inside, return -1.
        if(in_polyhedron(point) == CGAL::ON_UNBOUNDED_SIDE) {
            return 1;
        } else {
            return -1;
        }
    }

    Sphere_3 bounding_sphere() const {
        return bounding_sphere_;
    }

    Sphere_3 bounding_sphere_;
};


///////////////////////////////////////////////////////////////////////////////
// Labelled implicit function from vector of implicit functions              //
///////////////////////////////////////////////////////////////////////////////
//
// Define a class which takes a list of implicit functions.
// When queried if a point is inside the domain, it should check in order
// which domains the point is in and return the first matching domain
// from the list. The returned domain id is 1 indexed.
//
// This is what we defined the abstract function interface for.
//
template<typename Function, typename BGT>
class Implicit_vector_to_labeled_function_wrapper
{
public:
    typedef std::size_t return_type;
    typedef typename BGT::Point_3 Point_3;
    typedef std::vector<Function*> Function_vector;

    Implicit_vector_to_labeled_function_wrapper(
        const std::vector<Function*>& v
    ): function_vector_(v) {}

    // Return id of the first matching function.
    // Numbering starts from 1 because 0 is empty (ignored) space.
    return_type operator()(const Point_3& p, const bool = true) const {
        for(std::size_t i=0; i < function_vector_.size(); i++) {
            if((*function_vector_[i])(p) < 0) return i+1;
        }
        return 0;
    }

private:
    Function_vector function_vector_;
};



///////////////////////////////////////////////////////////////////////////////
// Mesh criteria classes                                                     //
///////////////////////////////////////////////////////////////////////////////
//
// We define some classes which can determine whether a tet is considered
// "bad". In our case, we want to define some minimum allowed edge length
// so we can properly sample within a given "exchange length".
//


// Surface_criteria ///////////////////////////////////////////////////////////
//
// Criteria class to allow setting arbitrary criteria for the surface mesher.
// The default Standard_criteria class had it's Criteria typedef protected
// and the Surface_mesh_default_criteria class didn't allow setting criteria
// directly to the criteria vector, only the criteria it predefined via its
// constructor.
//
template<class Criterion>
class Surface_criteria:
    public CGAL::Surface_mesher::Standard_criteria<Criterion>
{
public:
    typedef CGAL::Surface_mesher::Standard_criteria<Criterion> Base;

    typedef typename Base::Quality Quality;
    typedef typename Base::Criteria Criteria;

    Surface_criteria(): Base() {}
    Surface_criteria(const Criteria& c): Base(c) {}
};


// Edge_length_criterion /////////////////////////////////////////////////
//
// Criteria to set the maxmum allowed length of any edge.
//
template<typename Tr, typename Visitor_>
class Edge_length_criterion:
    public CGAL::Mesh_3::Abstract_criterion<Tr, Visitor_>
{
    typedef Edge_length_criterion<Tr, Visitor_> Self;

    typedef typename Tr::Cell_handle Cell_handle;
    typedef typename Tr::Facet Facet;
    typedef typename Tr::Geom_traits::FT FT;

    typedef CGAL::Mesh_3::Abstract_criterion<Tr, Visitor_> Base;
    typedef typename Base::Quality Quality;
    typedef typename Base::Badness Badness;

public:
    Edge_length_criterion(const FT& edge_length_bound):
        sq_edge_length_bound_(edge_length_bound*edge_length_bound)
    {}

    ~Edge_length_criterion(){}

protected:
    virtual void do_accept(Visitor_& v) const {
        v.visit(*this);
    }

    virtual Self* do_clone() const {
        return new Self(*this);
    }


    // Badness test for cells
    virtual Badness do_is_bad(const Cell_handle& ch) const {
        typedef typename Tr::Point Point_3;
        typedef typename Tr::Geom_traits Geom_traits;
        typedef typename Geom_traits::Compute_squared_distance_3 Distance;

        const Point_3& p = ch->vertex(0)->point();
        const Point_3& q = ch->vertex(1)->point();
        const Point_3& r = ch->vertex(2)->point();
        const Point_3& s = ch->vertex(3)->point();

        Distance distance = Geom_traits().compute_squared_distance_3_object();

        FT max_edge_length = distance(p,q);
        max_edge_length = (CGAL::max)(max_edge_length, distance(p,r));
        max_edge_length = (CGAL::max)(max_edge_length, distance(p,s));
        max_edge_length = (CGAL::max)(max_edge_length, distance(q,r));
        max_edge_length = (CGAL::max)(max_edge_length, distance(q,s));
        max_edge_length = (CGAL::max)(max_edge_length, distance(r,s));

        if(max_edge_length > sq_edge_length_bound_) {
            return Badness(Quality(sq_edge_length_bound_/max_edge_length));
        }

        return Badness();
    }


    // Badness test for facets
    virtual Badness do_is_bad(const Facet& fh) const {
        typedef typename Tr::Point Point_3;
        typedef typename Tr::Geom_traits Geom_traits;
        typedef typename Geom_traits::Compute_squared_distance_3 Distance;

        // Expect Facet handle is <Cell, int> shere cell is the cell and
        // int is the zero indexed id of the vertex *not* in the facet.
        const Cell_handle& ch = fh.first;
        const Point_3& p = ch->vertex((fh.second+1)%4)->point();
        const Point_3& q = ch->vertex((fh.second+2)%4)->point();
        const Point_3& r = ch->vertex((fh.second+3)%4)->point();

        Distance distance = Geom_traits().compute_squared_distance_3_object();

        FT max_edge_length = distance(p,q);
        max_edge_length = (CGAL::max)(max_edge_length, distance(p,r));
        max_edge_length = (CGAL::max)(max_edge_length, distance(q,r));

        if(max_edge_length > sq_edge_length_bound_) {
            return Badness(Quality(sq_edge_length_bound_/max_edge_length));
        }

        return Badness();
    }

private:
    FT sq_edge_length_bound_;
};



///////////////////////////////////////////////////////////////////////////////
// Define iterator projectors and predicates                                 //
///////////////////////////////////////////////////////////////////////////////
//
// We define an iterator for iterating over the finite points in
// a triangulation.
// We then define an iterator for iterating over the points in a triangulation
// that also belong to a given domain.
//


// Finite_points_iterator /////////////////////////////////////////////////////
//
// This iterator will iterate over the points given by the finite vertices
// in a triangulation.
//
template<typename Tr>
struct Finite_points_iterator {
    typedef CGAL::Project_point<typename Tr::Vertex> Project_point_;
    typedef
        CGAL::Iterator_project<
            typename Tr::Finite_vertices_iterator,
            Project_point_
        >
        type;
};


// Not In Domain Predicate ////////////////////////////////////////////////////
//
// This will return true iff the point p is not in the requested domain.
// Useful for the Filter_iterator class.
//
template<typename Is_in_domain>
struct NotInDomainPredicate {
    Is_in_domain const* is_in_domain;
    std::size_t value;

    NotInDomainPredicate(
        const Is_in_domain& is_in_domain,
        std::size_t value
    ):
        is_in_domain(&is_in_domain),
        value(value)
    {}
    
    template<typename Point_handle>
    bool operator()(const Point_handle& point_handle) {
        return (*is_in_domain)(*point_handle).get_value_or(0) != value;
    }
};



///////////////////////////////////////////////////////////////////////////////
// Type definitions                                                          //
///////////////////////////////////////////////////////////////////////////////
//
// Explicitly define the CGAL types we'll be using here to avoid
// ridiculously long types later, and to easily enough swap
// types out.
//


// The standard floating point Kernel
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;


// The polyhedron used for reconstruction
typedef CGAL::Mesh_polyhedron_3<Kernel>::type Polyhedron;


// Mesh Domains
typedef
    Implicit_vector_to_labeled_function_wrapper<FunctionBase<Kernel>, Kernel>
    Function_wrapper;
typedef
    CGAL::Labeled_mesh_domain_3<Function_wrapper, Kernel>
    Mesh_domain;
typedef
    CGAL::Implicit_surface_3<Kernel, Function_wrapper>
    Surface_mesh_domain;


// Triangulations
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr3;
typedef CGAL::Mesh_criteria_3<
    Tr3,
    CGAL::Mesh_edge_criteria_3<Tr3>,
    CGAL::Mesh_facet_criteria_3<Tr3>,
    CGAL::Mesh_cell_criteria_3<Tr3, CGAL::Mesh_3::Cell_criterion_visitor<Tr3> >
> Mesh_criteria;
typedef CGAL::Mesh_constant_domain_field_3<Mesh_domain::R, Mesh_domain::Index>
    Domain_sizing_field;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr3> C3t3;

typedef CGAL::Surface_mesh_default_triangulation_3 STr3;
typedef Surface_criteria<
    CGAL::Surface_mesher::Refine_criterion<STr3>
> Surface_mesh_criteria;
typedef CGAL::Surface_mesh_complex_2_in_triangulation_3<STr3> C2t3;



// To avoid verbose function and named parameters call
namespace param = CGAL::parameters;



// Predicates and iterators
typedef
    NotInDomainPredicate<Mesh_domain::Is_in_domain>
    Tr3_NotInDomainPredicate;
typedef
    CGAL::Filter_iterator<
        Finite_points_iterator<Tr3>::type,
        Tr3_NotInDomainPredicate
    >
    Tr3_SubdomainIterator;



// Stuff for validating mesh type options with program_options
struct MeshExtension {
    MeshExtension(std::string const& value): value(value) {}

    std::string value;

    typedef std::map<std::string,std::string> ExtensionMap;
    static ExtensionMap const& extension_descriptions() {
        static ExtensionMap extension_descriptions;

        if(!extension_descriptions.empty()) return extension_descriptions;

        extension_descriptions["xml"] =
            "Dolfin XML file including a Mesh and a MeshFunction. "
            "(volume only)";
        extension_descriptions["wyn"] =
            "Simple list of vertex values, followed by connectivities. "
            "(volume only)";
        extension_descriptions["neu"] =
            "PATRAN neutral file. "
            "(volume only)";
        extension_descriptions["off"] =
            "3D Mesh Object File Format. (surface only)";
        extension_descriptions["stl"] =
            "STereoLithography file. (surface only)";
        extension_descriptions["vtu"] =
            "VTK unstructured mesh file. (surface only)";

        return extension_descriptions;
    }
};

void validate(
    boost::any& v, std::vector<std::string> const& values,
    MeshExtension*, int
) {
    po::validators::check_first_occurrence(v);

    std::string const& ext = po::validators::get_single_string(values);

    if(MeshExtension::extension_descriptions().count(ext)) {
        v = boost::any(MeshExtension(ext));
    } else {
        throw po::invalid_option_value(ext);
    }
}


int main(int argc, char* argv[])
{

    ///////////////////////////////////////////////////////////////////////////
    // Set commandline options                                               //
    ///////////////////////////////////////////////////////////////////////////

    std::string usage(
        "Usage: meshinr [options] input output cell_size"
    );
    std::string description(
        "Smooth a domain defined by an INR file and mesh it with optional "
        "surrounding shell, useful for spatial transformation techniques "
        "in FEM codes. If cell domains are marked, the INR domain will be "
        "1, the extra space 2, and the spherical shell 3."
    );


    po::positional_options_description positional_options;
    positional_options.add("input",     1);
    positional_options.add("output",    1);
    positional_options.add("cell_size", 1);


    po::options_description required_options("Required");
    required_options.add_options() (
        "input,i", po::value<std::string>()->required(),
        "The input INR file."
    ) (
        "output,o", po::value<std::string>()->required(),
        "The output mesh file. Without the -t option, the type "
        "of mesh to output will be determined from the output "
        "file prefix."
    ) (
        "cell_size,c", po::value<double>()->required(),
        "The radius of the circumsphere of the mesh cells."
    );
    
    
    po::options_description output_options("Output File Types");
    output_options.add_options() (
        "mesh_file_type,m",
        po::value< std::vector<MeshExtension> >()->multitoken(),
        "The types of mesh to output. "
        "If this options is given, any extension for the -o file will "
        "be ignored and the given extensions will be appended to the name. "
        "Several file types may be given and each will be printed to a "
        "separate file. "
        "For more information, see --list_mesh_file_types."
    ) (
        "list_mesh_file_types", "List the allowed mesh filetypes."
    ) (
        "surface_mesh,S", po::value<bool>(),
        "Generate surface mesh. Default: true."
    ) (
        "volume_mesh,V", po::value<bool>(),
        "Generate volume mesh. Default: true."
    );


    po::options_description refinement_options("Mesh Refinement");
    refinement_options.add_options() (
        "refine_mesh,g", po::value<bool>(),
        "Refine mesh after generation. Default: true."
    );


    po::options_description shell_options("Bounding Shell");
    shell_options.add_options() (
        "shell,s",
        "Include a spherical shell around the INR domain."
    ) (
        "shell_cell_size,f", po::value<double>(),
        "The radius of the circumsphere of the cells in the shell and "
        "the extra space added to the INR domain. "
        "Implies -s. "
        "Default: cell_size."
    ) (
        "shell_thickness,t", po::value<double>(),
        "The thickness of the surrounding shell. "
        "Implies -s. "
        "Default: 4*shell_cell_size."
    ) (
        "shell_thickness_count,u", po::value<double>(),
        "The thickness of the surrounding shell in multiples "
        " of shell_cell_size. "
        "Implies -s."
    );


    po::options_description help_options("Help");
    help_options.add_options() (
        "help,h", "Print this help message"
    );


    po::options_description cmdline_options;
    cmdline_options
        .add(required_options)
        .add(output_options)
        .add(refinement_options)
        .add(shell_options)
        .add(help_options);


    po::variables_map args;

    try {
        po::store(
            po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .positional(positional_options)
                .run(),
            args
        );

        if(args.count("help")) {
            std::cout
                << std::endl
                << usage << std::endl << std::endl
                << description << std::endl
                << cmdline_options << std::endl;

            return EXIT_SUCCESS;
        }

        if(args.count("list_mesh_file_types")) {
            std::cout << std::endl;
            std::cout << "Mesh File Types:" << std::endl;
            for(
                MeshExtension::ExtensionMap::const_iterator it =
                    MeshExtension::extension_descriptions().begin();
                it != MeshExtension::extension_descriptions().end();
                ++it
            ) {
                std::cout << std::endl;
                std::cout << "Extension: " << it->first << std::endl;
                std::cout << "Description: " << it->second << std::endl;
            }
            std::cout << std::endl;

            return EXIT_SUCCESS;
        }

        po::notify(args);

    } catch(std::exception& e) {
        std::cout
            << std::endl
            << "Error: " << e.what() << std::endl
            << usage << std::endl;

        return EXIT_FAILURE;
    }



    ///////////////////////////////////////////////////////////////////////////


    // Sort out meshing and sizes etc.

    std::string input_filename = args["input"].as<std::string>();
    std::string output_filename = args["output"].as<std::string>();
    double particle_vertex_spacing = args["cell_size"].as<double>();


    bool include_bounding_spheres = false;

    if(args.count("shell")) {
        include_bounding_spheres = true;
    }


    double space_vertex_spacing = args["cell_size"].as<double>();

    if(args.count("shell_cell_size")) {
        include_bounding_spheres = true;
        space_vertex_spacing = args["shell_cell_size"].as<double>();
    }


    double shell_thickness = 4*space_vertex_spacing;

    if(args.count("shell_thickness")) {
        include_bounding_spheres = true;
        shell_thickness = args["shell_thickness"].as<double>();
    }

    if(args.count("shell_thickness_count")) {
        include_bounding_spheres = true;
        shell_thickness =
            space_vertex_spacing*args["shell_thickness"].as<double>();
    }


    // Sort out mesh output types
    std::set<std::string> mesh_extensions;

    if(args.count("mesh_file_type")) {
        // We have mesh_file_types, populate the extensions from this list
        std::vector<MeshExtension> const& me =
            args["mesh_file_type"].as< std::vector<MeshExtension> >();

        for(
            std::vector<MeshExtension>::const_iterator it = me.begin();
            it != me.end();
            ++it
        ) {
            mesh_extensions.insert(it->value);
        }

    } else {
        // No explicit file types given, find extension from output file
        std::size_t extpos = output_filename.rfind('.');
        if(extpos == std::string::npos) {
            std::cout
                << "Error: Expected output filename to include extension!"
                << std::endl << std::endl
                << usage << std::endl << std::endl;

            return EXIT_FAILURE;
        }

        std::string ext = output_filename.substr(extpos+1);
        if(!MeshExtension::extension_descriptions().count(ext)) {
            std::cout
                << "Error: Unknown extension " << ext << std::endl << std::endl
                << usage << std::endl << std::endl;

            return EXIT_FAILURE;
        }

        // Strip extension from output filename so it can be treated as
        // a base name later.
        output_filename.erase(extpos);
        mesh_extensions.insert(ext);
    }


    // Sort mesh generation
    bool generate_surface_mesh = true;
    bool generate_volume_mesh  = true;

    if(args.count("surface_mesh")) {
        generate_surface_mesh = args["surface_mesh"].as<bool>();
    }

    if(args.count("volume_mesh")) {
        generate_volume_mesh = args["volume_mesh"].as<bool>();
    }


    // Sort mesh refinement
    bool refine_mesh = true;

    if(args.count("refine_mesh")) {
        refine_mesh = args["refine_mesh"].as<bool>();
    }


    // Print arguments

    // File types
    std::cout
        << std::endl
        << "Configuration:" << std::endl
        << "Input filename: " << input_filename << std::endl
        << "Output filenames: ";
    for(
        std::set<std::string>::iterator
            it = mesh_extensions.begin(),
            it_e = (--mesh_extensions.end());
        it != mesh_extensions.end();
        ++it
    ) {
        std::cout << output_filename << "." << *it;
        if(it != it_e) std::cout << ", ";
    }

    // Cell sizes, include shell
    std::cout
        << std::endl
        << "INR domain cell size: " << particle_vertex_spacing << std::endl
        << "Include shell: "
            << (include_bounding_spheres? "yes":"no") << std::endl;
    if(include_bounding_spheres) {
        std::cout
            << "Space/shell cell size: " << space_vertex_spacing << std::endl
            << "Shell thickness: " << shell_thickness << std::endl;
    }

    // Mesh generation
    std::cout
        << "Generating Surface Mesh: "
            << (generate_surface_mesh?"yes":"no") << std::endl
        << "Generating Volume Mesh: "
            << (generate_volume_mesh?"yes":"no") << std::endl;

    std::cout << std::endl;




    //
    // Generate a smooth domain from the INR file
    //
    std::cout << "Generating a smooth mesh domain..." << std::endl;

    // Loads image
    std::cout << std::endl << "- Loading INR file..." << std::endl;
    CGAL::Image_3 image;
    image.read(input_filename.c_str());
    std::cout << "-- Done" << std::endl;


    // Output unreconstructed surface to stl file 
    std::cout << "- Outputting unreconstructed stl file..." << std::endl;
    {
        std::ofstream image_stl(
            (output_filename + ".unreconstructed.stl").c_str()
        );
        output_image_3_to_stl(image_stl, image);
    }
    std::cout << "-- Done." << std::endl;


    // Generate reconstruction_polyhedron
    std::cout << "- Generating smooth domain..." << std::endl;
    Polyhedron reconstruction_polyhedron;
    make_smooth_polyhedron_from_discrete_image(
        reconstruction_polyhedron, image, 0.5*particle_vertex_spacing
    );
    std::cout << "-- Done" << std::endl;


    // Output reconstructed surface to stl file
    {
        std::ofstream polyhedron_stl(
            (output_filename + ".reconstructed.stl").c_str()
        );
        output_polyhedron_to_stl(
            polyhedron_stl, reconstruction_polyhedron
        );
    }


    // Generate a polyhedron domain function
    PolyhedronFunction<Polyhedron> reconstructed_domain(
        reconstruction_polyhedron
    );


    ///////////////////////////////////////////////////////////////////////////
    // Generate the meshes                                                   //
    ///////////////////////////////////////////////////////////////////////////
    std::cout << std::endl << "Generating The Meshes..." << std::endl;


    //
    // Generate labelled function for the reconstructed surface
    //
    Function_wrapper::Function_vector domain_functions;
    domain_functions.push_back(&reconstructed_domain);

    
    // Get/set cell parameters

    // Set cell size from required vertex spacing
    // Expect ~10 nm vertex neighbour distance
    double particle_cell_size = particle_vertex_spacing;
    double space_cell_size    = space_vertex_spacing;

    // Set facet distance to a sub-cell size.
    // It should be 0.16x the feature size of the domain...?
    double particle_facet_distance = particle_cell_size*0.5*0.16;
    double space_facet_distance    = space_cell_size*0.5*0.16;


    std::cout << "- Cell Size: " << particle_cell_size << std::endl;
    std::cout << "- Facet Distance: " << particle_facet_distance << std::endl;


    //
    // Generate a surface mesh
    //
    if(generate_surface_mesh) {

        std::cout << "- Generating Surface Mesh..." << std::endl;

        // Define mesh domain
        Surface_mesh_domain surface_mesh_domain(
            domain_functions,
            Kernel::Sphere_3(
                reconstructed_domain.bounding_sphere().center(),
                10*reconstructed_domain.bounding_sphere().squared_radius()
            ),
            particle_facet_distance/1000.
        );

        // Define mesh criteria
        CGAL::Surface_mesher::Aspect_ratio_criterion<STr3>
            angle_bound_criterion(25);
        CGAL::Surface_mesher::Edge_size_criterion<STr3>
            edge_size_criterion(2*particle_cell_size);
        CGAL::Surface_mesher::Curvature_size_criterion<STr3>
            facet_distance_criterion(particle_facet_distance);

        Surface_mesh_criteria::Criteria criteria;
        criteria.push_back(&angle_bound_criterion);
        criteria.push_back(&edge_size_criterion);
        criteria.push_back(&facet_distance_criterion);

        Surface_mesh_criteria surface_mesh_criteria(criteria);

        std::cout << "-- Generating Initial Mesh..." << std::endl;
        STr3 str3;
        C2t3 c2t3(str3);
        CGAL::make_surface_mesh(
            c2t3, surface_mesh_domain, surface_mesh_criteria,
            CGAL::Manifold_tag(),
            20
        );
        std::cout << "--- Done" << std::endl;

        // Do iterative refinement
        std::cout << "-- Refining Mesh..." << std::endl;
        std::cout
            << "--- Target Average Edge Size: " << particle_cell_size
            << std::endl;

        std::size_t refine_count = 0;
        while(true) {

            // Find the current edge average
            double edge_average = 0.0;
            std::size_t edge_count = 0;
            for(
                C2t3::Edge_iterator eit = c2t3.edges_begin();
                eit != c2t3.edges_end();
                eit++
            ) {
                if(c2t3.face_status(*eit) == C2t3::REGULAR) {
                    const STr3::Point& a =
                        eit->first->vertex(eit->second)->point();
                    const STr3::Point& b =
                        eit->first->vertex(eit->third)->point();

                    edge_average += std::sqrt(CGAL::squared_distance(a, b));
                    edge_count++;
                }
                else {
                    std::cout << "Not regular!" << std::endl;
                }
            }
            edge_average /= edge_count;

            std::cout
                << "-- Current Edge Average: " << edge_average << std::endl;

            // Check edge average is within tolerance
            if(edge_average <= particle_cell_size) {
                std::cout
                    << "--- Requested edge average met at "
                        << 2-refine_count/10.
                        << "*particle_cell_size"
                        << std::endl;
                break;
            }

            // Tick down the current edge size criterion
            edge_size_criterion.set_bound(
                (2  -(refine_count+1)/10.)*particle_cell_size
            );

            
            Surface_mesh_criteria updated_surface_mesh_criteria(criteria);

            // Run surface mesher again
            std::cout
                << "--- Refining at "
                << (2  -(refine_count+1)/10.)
                << "*particle_cell_size ..."
                << std::endl;
            CGAL::make_surface_mesh(
                c2t3, surface_mesh_domain, updated_surface_mesh_criteria,
                CGAL::Manifold_tag(),
                0
            );
            std::cout << "---- Done" << std::endl;

            refine_count++;

        }

        std::cout << "-- Done" << std::endl;


        // Find the bounding sphere of the surface mesh
        std::cout << "- Finding minimum bounding sphere..." << std::endl;
        Kernel::Sphere_3 surface_sphere = compute_bounding_sphere<Kernel>(
            Finite_points_iterator<STr3>::type(
                c2t3.triangulation().finite_vertices_begin()
            ),
            Finite_points_iterator<STr3>::type(
                c2t3.triangulation().finite_vertices_end()
            )
        );
        std::cout << "-- Surface bounding sphere: "
            << "("
                << surface_sphere.center().x() << ", "
                << surface_sphere.center().y() << ", "
                << surface_sphere.center().z()
            << ") "
            << std::sqrt(surface_sphere.squared_radius())
            << std::endl;

        // Center the mesh on the bounding sphere
        std::cout << "- Centering the surface mesh..." << std::endl;
        Kernel::Vector_3 surface_center_offset =
            CGAL::ORIGIN - surface_sphere.center();
        for(
            STr3::Finite_vertices_iterator vit =
                c2t3.triangulation().finite_vertices_begin();
            vit != c2t3.triangulation().finite_vertices_end();
            vit++
        ) {
            vit->point() = vit->point() + surface_center_offset;
        }

        // Recompute the bounding sphere
        std::cout << "- Recomputing bounding sphere" << std::endl;
        Kernel::Sphere_3 new_surface_sphere = compute_bounding_sphere<Kernel>(
            Finite_points_iterator<STr3>::type(
                c2t3.triangulation().finite_vertices_begin()
            ),
            Finite_points_iterator<STr3>::type(
                c2t3.triangulation().finite_vertices_end()
            )
        );
        std::cout << "-- Surface bounding sphere: "
            << "("
                << new_surface_sphere.center().x() << ", "
                << new_surface_sphere.center().y() << ", "
                << new_surface_sphere.center().z()
            << ") "
            << std::sqrt(new_surface_sphere.squared_radius())
            << std::endl;


        std::cout
            << "- Surface Vertices: "
            << str3.number_of_vertices()
            << std::endl;
        std::cout
            << "- Surface Facets: "
            << str3.number_of_finite_facets()
            << std::endl;


        // Output surfaces
        std::cout << "- Writing Surface Meshes..." << std::endl;

        // OFF file
        if(mesh_extensions.count("off")) {
            // OFF file
            std::cout << "-- Writing OFF file..." << std::endl;
            std::cout
                << "--- Filename: "
                << (output_filename + ".surface.off").c_str()
                << std::endl;
            std::ofstream surface_off_file(
                (output_filename + ".surface.off").c_str()
            );
            surface_off_file << std::scientific;
            surface_off_file.precision(16);

            if(surface_off_file) {
                CGAL::output_surface_facets_to_off(surface_off_file, c2t3);
            }
            
            if(!surface_off_file) {
                std::cout << "--- File operation failed!" << std::endl;
                std::cout << "---- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "--- Done" << std::endl;
        }

        // VTU file
        if(mesh_extensions.count("vtu")) {
            Polyhedron surface_polyhedron;
            CGAL::output_surface_facets_to_polyhedron(c2t3, surface_polyhedron);

            std::cout << "-- Writing VTU file..." << std::endl;
            std::cout
                << "--- Filename: "
                << (output_filename + ".surface.vtu").c_str()
                << std::endl;
            std::ofstream surface_vtu_file(
                (output_filename + ".surface.vtu").c_str()
            );
            surface_vtu_file << std::scientific;
            surface_vtu_file.precision(16);

            if(surface_vtu_file) {
                output_polyhedron_to_vtu(
                    surface_vtu_file, surface_polyhedron
                );
            }
            
            if(!surface_vtu_file) {
                std::cout << "--- File operation failed!" << std::endl;
                std::cout << "---- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "--- Done" << std::endl;
        }

        // STL file
        if(mesh_extensions.count("stl")) {
            Polyhedron surface_polyhedron;
            CGAL::output_surface_facets_to_polyhedron(c2t3, surface_polyhedron);

            std::cout << "-- Writing STL file..." << std::endl;
            std::cout
                << "--- Filename: "
                << (output_filename + ".surface.stl").c_str()
                << std::endl;
            std::ofstream surface_stl_file(
                (output_filename + ".surface.stl").c_str()
            );
            surface_stl_file << std::scientific;
            surface_stl_file.precision(16);

            if(surface_stl_file) {
                output_polyhedron_to_stl(
                    surface_stl_file, surface_polyhedron
                );
            }
            
            if(!surface_stl_file) {
                std::cout << "--- File operation failed!" << std::endl;
                std::cout << "---- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "--- Done" << std::endl;
        }
        std::cout << "-- Done" << std::endl;

        std::cout << "- Done" << std::endl;

    } // if(generate_surface_mesh)



    //
    // Generate a volume mesh
    //
    if(generate_volume_mesh) {

        std::cout << std::endl << "Generating Volume Mesh..." << std::endl;


        // Particle Only Mesh Domain
        Mesh_domain particle_domain(
            domain_functions,
            Kernel::Sphere_3(
                reconstructed_domain.bounding_sphere().center(),
                4*reconstructed_domain.bounding_sphere().squared_radius()
            ),
            particle_facet_distance/1000.
        );


        if(include_bounding_spheres) {

            // First bounding sphere should be just a little larger than
            // the particle
            double f2_radius2 =
                reconstructed_domain.bounding_sphere().squared_radius();
            f2_radius2 = std::sqrt(f2_radius2);
            // Add a little space between the INR domain and the inner sphere
            f2_radius2 += particle_cell_size;
            f2_radius2 = std::pow(f2_radius2, 2);
            KernelFunction<Kernel::Sphere_3> f2(
                Kernel::Sphere_3(
                    reconstructed_domain.bounding_sphere().center(),
                    f2_radius2
                ),
                Kernel::Sphere_3(
                    reconstructed_domain.bounding_sphere().center(),
                    f2_radius2
                )
            );
            std::cout
                << "- Expected Inner Sphere Radius: "
                << std::sqrt(f2.function.squared_radius())
                << std::endl;
            domain_functions.push_back(&f2);

            // The surface of the second bounding sphere should be about
            // shell_thickness/space_vertex_spacing tets away from the first.
            // Maybe about 4 tets?
            double f3_radius2 = f2.function.squared_radius();
            f3_radius2 = std::sqrt(f3_radius2);
            f3_radius2 += shell_thickness;
            f3_radius2 = std::pow(f3_radius2, 2);
            KernelFunction<Kernel::Sphere_3> f3(
                Kernel::Sphere_3(
                    f2.function.center(),
                    f3_radius2
                ),
                Kernel::Sphere_3(
                    f2.function.center(),
                    f3_radius2
                )
            );
            std::cout
                << "- Expected Outer Sphere Radius: "
                << std::sqrt(f3.function.squared_radius())
                << std::endl;
            domain_functions.push_back(&f3);

            std::cout
                << "- Estimated Cell Count: "
                << (3.14159*std::pow(f3_radius2, 3/2.))
                    / std::pow(particle_cell_size,3)
                << std::endl;
        }



        // Full Mesh Domain
        Mesh_domain full_domain(
            domain_functions,
            domain_functions.back()->bounding_sphere(),
            particle_facet_distance/1000.
        );
        Mesh_domain::Is_in_domain domain_index =
            full_domain.is_in_domain_object();

        //
        // Mesh criteria
        //
        typedef
            Edge_length_criterion<
                Tr3, Mesh_criteria::Cell_criteria::Visitor
            >
            CEL_criterion;
        typedef
            Edge_length_criterion<
                Tr3, Mesh_criteria::Facet_criteria::Visitor
            >
            FEL_criterion;

        // Particle criteria
        Mesh_criteria::Facet_criteria particle_facet_criteria(
            25, Tr3::Geom_traits::FT(0), particle_facet_distance
        );
        particle_facet_criteria.add(new FEL_criterion(2*particle_cell_size));
        Mesh_criteria::Cell_criteria particle_cell_criteria(
            2.01, Tr3::Geom_traits::FT(0)
        );
        particle_cell_criteria.add(new CEL_criterion(2*particle_cell_size));

        Mesh_criteria particle_criteria(
            particle_facet_criteria,
            particle_cell_criteria
        );


        // Space criteria
        // Main difference is the facet distance
        Mesh_criteria::Facet_criteria space_facet_criteria(
            25, Tr3::Geom_traits::FT(0), space_facet_distance
        );
        space_facet_criteria.add(new FEL_criterion(2*space_cell_size));
        Mesh_criteria::Cell_criteria space_cell_criteria(
            2.01, Tr3::Geom_traits::FT(0)
        );
        space_cell_criteria.add(new CEL_criterion(2*space_cell_size));

        Mesh_criteria space_criteria(
            space_facet_criteria,
            space_cell_criteria
        );

        // Generate mesh

        // Generate the initial particle mesh with strict criteria
        std::cout << "- Generating Particle Mesh..." << std::endl;

        // Generate initial mesh
        std::cout << "-- Generating Initial Particle Mesh..." << std::endl;
        C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(
            particle_domain, particle_criteria,
            param::no_odt(), param::no_lloyd(),
            param::no_perturb(), param::no_exude()
        );
        std::cout << "--- Done" << std::endl;

        // Refine mesh with stricter edge length bound until the average
        // edge length is within the requested size, within 10 steps.
        // This assumes that setting the edge length bound to
        // particle_cell_size will definitely result in smaller edges than
        // particle_cell_size.
        std::cout << "-- Refining Particle Mesh..." << std::endl;
        for(std::size_t i=1; i<=10; i++) {
            const Tr3& tr3 = c3t3.triangulation();

            // Find edge length average
            double edge_average = 0.0;
            for(
                Tr3::Finite_edges_iterator eit =
                    tr3.finite_edges_begin();
                eit != tr3.finite_edges_end();
                eit++
            ) {
                if(eit->first->subdomain_index() != 1) {
                    //std::cout
                    //    << "Subdomain index: "
                    //        << eit->first->subdomain_index()
                    //        << std::endl;
                    continue;
                }

                const Tr3::Point& p1 = eit->first->vertex(eit->second)->point();
                const Tr3::Point& p2 = eit->first->vertex(eit->third)->point();

                double len = std::sqrt(CGAL::squared_distance(p1, p2));

                edge_average += len;
            }
            edge_average /= tr3.number_of_finite_edges();

            std::cout
                << "--- Edge average " << (i-1) << ": "
                    << edge_average
                << std::endl;

            // If the edge average is wthin tolerance, break the loop
            // and let's do the rest of the meshing
            if(edge_average <= particle_cell_size) {
                std::cout
                    << std::endl
                    << "--- Requested edge average met at "
                        << 2-(i-1)/10.
                        << "*particle_cell_size"
                        << std::endl;
 
                break;
            }


            // Edges are still too large.
            // Let's make the edge bound smaller.
            Mesh_criteria::Facet_criteria updated_particle_facet_criteria(
                particle_facet_criteria
            );
            updated_particle_facet_criteria.add(
                new FEL_criterion((1 + (1.-i/10.))*particle_cell_size)
            );
            Mesh_criteria::Cell_criteria updated_particle_cell_criteria(
                particle_cell_criteria
            );
            updated_particle_cell_criteria.add(
                new CEL_criterion((1 + (1.-i/10.))*particle_cell_size)
            );

            Mesh_criteria updated_particle_criteria(
                updated_particle_facet_criteria,
                updated_particle_cell_criteria
            );

            // Rerun the meshing
            std::cout
                << "--- Refining at "
                    << (1+(1.-i/10.)) << "*particle_cell_size ..."
                    << std::endl;
            CGAL::refine_mesh_3(
                c3t3, particle_domain,
                updated_particle_criteria,
                param::no_odt(), param::no_lloyd(),
                param::no_perturb(), param::no_exude(),
                param::no_reset_c3t3()
            );
            std::cout << "---- Done" << std::endl;
        }
        std::cout << "--- Done" << std::endl;

        // Finished generating particle mesh
        std::cout << "-- Done" << std::endl;

        // Generate the surrounding space with relaxed space criteria
        std::cout << "- Generating Full Mesh... " << std::endl;
        CGAL::refine_mesh_3(
            c3t3, full_domain, space_criteria,
            param::no_odt(), param::no_lloyd(),
            param::no_perturb(), param::no_exude(),
            param::no_reset_c3t3()
        );
        std::cout << "-- Done" << std::endl;

        // Refine the mesh
        if(refine_mesh) {
            std::cout << "- Optimizing Mesh..." << std::endl;

            // Freeze vertices on the boundaries before optimisation
            std::cout << "-- Freezing boundary vertices..." << std::endl;
            for(
                C3t3::Triangulation::Finite_facets_iterator fit =
                    c3t3.triangulation().finite_facets_begin();
                fit != c3t3.triangulation().finite_facets_end();
                fit++
            ) {
                if(fit->first->is_facet_on_surface(fit->second)) {
                    for(std::size_t i=0; i<3; i++) {
                        fit->first->vertex(
                            c3t3.triangulation().vertex_triple_index(
                                fit->second, i
                            )
                        )->set_dimension(0);
                    }
                }
            }
            std::cout << "--- Done" << std::endl;

            std::cout << "-- Running Optimization..." << std::endl;
            CGAL::refine_mesh_3(
                c3t3, full_domain, space_criteria,
                param::odt(),
                param::lloyd(),
                param::perturb(param::time_limit=Kernel::FT(0)),
                param::exude(param::time_limit=Kernel::FT(0)),
                param::no_reset_c3t3()
            );
            std::cout << "--- Done" << std::endl;

            std::cout << "-- Done" << std::endl;
        }

        // Mesh generated.
        std::cout << "- Done" << std::endl;




        ///////////////////////////////////////////////////////////////////////
        // Output some statistics                                            //
        ///////////////////////////////////////////////////////////////////////

        // Get the mesh triangulation
        const Tr3& tr3 = c3t3.triangulation();

        std::cout
            << "- Final Vertex Count: "
                << tr3.number_of_vertices() 
            << std::endl
            << "- Final Cell Count:   "
                << tr3.number_of_finite_cells()
            << std::endl;

        double edge_max = std::numeric_limits<double>::min();
        double edge_min = std::numeric_limits<double>::max();
        double edge_average = 0.0;
        double edge_std_dev = 0.0;
        std::size_t edges_gt_set_max = 0.0;
        std::size_t edges_lte_set_max = 0.0;
        for(
            Tr3::Finite_edges_iterator eit =
                tr3.finite_edges_begin();
            eit != tr3.finite_edges_end();
            eit++
        ) {

            const Tr3::Point& p1 = eit->first->vertex(eit->second)->point();
            const Tr3::Point& p2 = eit->first->vertex(eit->third)->point();

            // Only interested in edge sizes inside the particle
            if(eit->first->subdomain_index() != 1) continue;

            double len = std::sqrt(CGAL::squared_distance(p1, p2));

            if(edge_max < len) edge_max = len;
            if(edge_min > len) edge_min = len;
            edge_average += len;

            if(len >  particle_cell_size){
                edges_gt_set_max++;
            } else {
                edges_lte_set_max++;
            }
        }
        edge_average /= tr3.number_of_finite_edges();
        for(
            Tr3::Finite_edges_iterator eit =
                tr3.finite_edges_begin();
            eit != tr3.finite_edges_end();
            eit++
        ) {

            const Tr3::Point& p1 = eit->first->vertex(eit->second)->point();
            const Tr3::Point& p2 = eit->first->vertex(eit->third)->point();

            // Only interested in edge sizes inside the particle
            if(eit->first->subdomain_index() != 1) continue;

            double len = std::sqrt(CGAL::squared_distance(p1, p2));

            edge_std_dev += (len-edge_average)*(len-edge_average);
        }
        edge_std_dev = std::sqrt(edge_std_dev/tr3.number_of_finite_edges());

        std::cout
            << "- Final Particle Edge Sizes:" << std::endl
            << "-- Max: " << edge_max << std::endl
            << "-- Min: " << edge_min << std::endl
            << "-- Average: " << edge_average << std::endl
            << "-- Deviation: " << edge_std_dev << std::endl
            << "-- Num Over Max Edge Size: " << edges_gt_set_max << std::endl
            << "-- Num Under Max Edge Size: " << edges_lte_set_max << std::endl
            << "-- Total Edges: " << tr3.number_of_finite_edges()
            << std::endl;


        ///////////////////////////////////////////////////////////////////////
        // Center mesh on outer bounding sphere                              //
        ///////////////////////////////////////////////////////////////////////
        //
        // Find the inner and outer bounding spheres for the mesh,
        // then center the mesh on the outer sphere.
        // In Micromag, it expects the bounding spheres to be concentric
        // on the origin. From the definition of the spatial transformation,
        // if the radius given for the outer ring is smaller than the actual
        // radius, a division by zero is performed, and the program often
        // crashes.
        //


        std::cout << "- Finding bounding spheres..." << std::endl;


        // Find the initial bounding spheres //////////////////////////////////
        std::cout << "-- Finding inner sphere" << std::endl;
        Kernel::Sphere_3 inner_sphere = compute_bounding_sphere<Kernel>(
            Tr3_SubdomainIterator(
                tr3.finite_vertices_end(),
                Tr3_NotInDomainPredicate(domain_index, 2),
                tr3.finite_vertices_begin()
            ),
            Tr3_SubdomainIterator(
                tr3.finite_vertices_end(),
                Tr3_NotInDomainPredicate(domain_index, 2)
            )
        );

        std::cout << "-- Finding outer sphere..." << std::endl;
        Kernel::Sphere_3 outer_sphere = compute_bounding_sphere<Kernel>(
            Finite_points_iterator<Tr3>::type(
                Tr3::Finite_vertices_iterator(
                    tr3.finite_vertices_begin()
                )
            ),
            Finite_points_iterator<Tr3>::type(
                Tr3::Finite_vertices_iterator(
                    tr3.finite_vertices_end()
                )
            )
        );
        std::cout << "-- Inner Sphere: ("
            << inner_sphere.center() << "), "
            << std::sqrt(inner_sphere.squared_radius())
            << std::endl;
        std::cout << "-- Outer Sphere: ("
            << outer_sphere.center() << "), "
            << std::sqrt(outer_sphere.squared_radius())
            << std::endl;


        // Compute deviation of sphere radii as measured from 0.0 /////////////
        double sphere_radius_average = 0.0;
        double sphere_radius_max = 0.0;
        double sphere_radius_deviation = 0.0;
        double sphere_radius_max_deviation = 0.0;

        Tr3::Point sphere_center(
            domain_functions.back()->bounding_sphere().center()
        );
        //Tr3::Point sphere_center(0.0,0.0,0.0); 

        // Find outer sphere points
        std::set<Tr3::Vertex_handle> surface_vertex_list;
        for(
            Tr3::Finite_facets_iterator fit =
                tr3.finite_facets_begin();
            fit != tr3.finite_facets_end();
            fit++
        ) {
            if(
                fit->first->is_facet_on_surface(fit->second)
                && fit->first->subdomain_index() == 3
                && tr3.mirror_facet(*fit).first->subdomain_index() == 0
            ) {
                for(std::size_t i=0; i<3; i++) {
                    const Tr3::Vertex_handle& vh = fit->first->vertex(
                        tr3.vertex_triple_index(fit->second, i)
                    );
                    surface_vertex_list.insert(vh);
                }
            }
        }

        // Find average
        for(
            std::set<Tr3::Vertex_handle>::iterator vit
                = surface_vertex_list.begin();
            vit != surface_vertex_list.end();
            vit++
        ) {
            double val = std::sqrt(CGAL::squared_distance(
                (**vit).point(), sphere_center
            ));

            sphere_radius_average += val;

            if(sphere_radius_max < val) sphere_radius_max = val;
        }
        sphere_radius_average /= surface_vertex_list.size();

        // Find standard deviation (and max deviation)
        for(
            std::set<Tr3::Vertex_handle>::iterator vit
                = surface_vertex_list.begin();
            vit != surface_vertex_list.end();
            vit++
        ) {
            double dist = std::sqrt(CGAL::squared_distance(
                (**vit).point(), sphere_center
            ));
            double val = dist - sphere_radius_average;

            if(sphere_radius_max_deviation < val) {
                sphere_radius_max_deviation = val;
            }

            sphere_radius_deviation += val*val;
        }
        sphere_radius_deviation = std::sqrt(
            sphere_radius_deviation/surface_vertex_list.size()
        );

        std::cout
            << "-- Outer radius deviation:" << std::endl
            << "--- Num Points: " << surface_vertex_list.size() << std::endl
            << "--- Average: " << sphere_radius_average << std::endl
            << "--- Deviation: " << sphere_radius_deviation << std::endl
            << "--- Deviation Max: "
                << sphere_radius_max_deviation << std::endl;


        // Center the mesh on the outer sphere ////////////////////////////////
        std::cout << "- Centering the mesh on the outer sphere..." << std::endl;

        Kernel::Vector_3 center_offset = CGAL::ORIGIN - outer_sphere.center();
        for(
            Tr3::Finite_vertices_iterator vit =
                tr3.finite_vertices_begin();
            vit != tr3.finite_vertices_end();
            vit++
        ) {
            vit->point() = vit->point() + center_offset;
        }


        std::cout << "-- Finding outer sphere..." << std::endl;
        outer_sphere = compute_bounding_sphere<Kernel>(
            Finite_points_iterator<Tr3>::type(
                Tr3::Finite_vertices_iterator(
                    tr3.finite_vertices_begin()
                )
            ),
            Finite_points_iterator<Tr3>::type(
                Tr3::Finite_vertices_iterator(
                    tr3.finite_vertices_end()
                )
            )
        );
        std::cout << "-- Outer Sphere: ("
            << outer_sphere.center() << "), "
            << std::sqrt(outer_sphere.squared_radius())
            << std::endl;


        // Compute deviation of sphere radii as measured from 0.0 /////////////
        sphere_radius_average = 0.0;
        sphere_radius_max = 0.0;
        sphere_radius_deviation = 0.0;
        sphere_radius_max_deviation = 0.0;

        sphere_center = Tr3::Point(0.0,0.0,0.0);

        // Find outer sphere points
        surface_vertex_list.clear();
        for(
            Tr3::Finite_facets_iterator fit =
                tr3.finite_facets_begin();
            fit != tr3.finite_facets_end();
            fit++
        ) {
            if(
                fit->first->is_facet_on_surface(fit->second)
                && fit->first->subdomain_index() == 3
                && tr3.mirror_facet(*fit).first->subdomain_index() == 0
            ) {
                for(std::size_t i=0; i<3; i++) {
                    const Tr3::Vertex_handle& vh = fit->first->vertex(
                        tr3.vertex_triple_index(fit->second, i)
                    );
                    surface_vertex_list.insert(vh);
                }
            }
        }

        // Find average
        for(
            std::set<Tr3::Vertex_handle>::iterator vit
                = surface_vertex_list.begin();
            vit != surface_vertex_list.end();
            vit++
        ) {
            double val = std::sqrt(CGAL::squared_distance(
                (**vit).point(), sphere_center
            ));

            sphere_radius_average += val;

            if(sphere_radius_max < val) sphere_radius_max = val;
        }
        sphere_radius_average /= surface_vertex_list.size();

        // Find standard deviation (and max deviation)
        for(
            std::set<Tr3::Vertex_handle>::iterator vit
                = surface_vertex_list.begin();
            vit != surface_vertex_list.end();
            vit++
        ) {
            double dist = std::sqrt(CGAL::squared_distance(
                (**vit).point(), sphere_center
            ));
            double val = dist - sphere_radius_average;

            if(sphere_radius_max_deviation < val) {
                sphere_radius_max_deviation = val;
            }

            sphere_radius_deviation += val*val;
        }
        sphere_radius_deviation = std::sqrt(
            sphere_radius_deviation/surface_vertex_list.size()
        );

        std::cout
            << "-- Outer radius deviation:" << std::endl
            << "--- Num Points: " << surface_vertex_list.size() << std::endl
            << "--- Average: " << sphere_radius_average << std::endl
            << "--- Deviation: " << sphere_radius_deviation << std::endl
            << "--- Deviation Max: "
                << sphere_radius_max_deviation << std::endl;




        ///////////////////////////////////////////////////////////////////////
        // Output                                                            //
        ///////////////////////////////////////////////////////////////////////

        std::cout << std::endl << "Writing Mesh Files..." << std::endl;


        // Output simple vertex-connectivity format ///////////////////////////
        if(mesh_extensions.count("wyn")) {
            std::cout << "- Writing Wyn File." << std::endl;
            std::cout
                << "-- Filename: "
                << (output_filename + ".wyn").c_str()
                << std::endl;

            std::ofstream wyn_file((output_filename + ".wyn").c_str());
            wyn_file << std::scientific;
            wyn_file.precision(16);

            if(wyn_file) {

                // Use reference to avoid changing vertex handle iterators
                typedef Tr3::Finite_vertices_iterator FVit;

                wyn_file
                    << "Vertices: " << tr3.number_of_vertices() << std::endl
                    << "Cells: " << tr3.number_of_finite_cells() << std::endl;

                std::map<Tr3::Vertex_handle, std::size_t> vertex_to_index;

                // Generate vertex map and output vertices to disk
                std::size_t current_vertex_index = 0;
                for(
                    FVit fvit = tr3.finite_vertices_begin();
                    fvit != tr3.finite_vertices_end();
                    fvit++, current_vertex_index++
                ) {
                    vertex_to_index[fvit] = current_vertex_index;

                    Tr3::Point p = fvit->point();

                    wyn_file
                        << p.x() << " " << p.y() << " " << p.z()
                        << std::endl;
                }

                // Output Cell connectivities
                for(
                    Tr3::Finite_cells_iterator cit = tr3.finite_cells_begin();
                    cit != tr3.finite_cells_end();
                    cit++
                ) {
                    for(std::size_t i=0; i<4; i++) {
                        wyn_file << vertex_to_index[cit->vertex(i)];
                        if(i!=3) wyn_file << " ";
                    }
                    wyn_file << std::endl;
                }

            }
            
            if(!wyn_file) {
                std::cout << "-- File operation failed!" << std::endl;
                std::cout << "--- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "-- Done" << std::endl;
        }


        // Output to Dolfin XML ///////////////////////////////////////////////
        if(mesh_extensions.count("xml")) {
            std::cout << "- Writing Dolfin XML File..." << std::endl;
            std::cout
                << "-- Filename: "
                << (output_filename + ".xml").c_str()
                << std::endl;

            std::ofstream xml_file((output_filename + ".xml").c_str());
            xml_file << std::scientific;
            xml_file.precision(16);

            if(xml_file) {
                output_c3t3_to_dolfin_xml(xml_file, c3t3);
            }
            
            if(!xml_file) {
                std::cout << "-- File operation failed!" << std::endl;
                std::cout << "--- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "-- Done" << std::endl;
        }



        // Output to Patran ///////////////////////////////////////////////////
        if(mesh_extensions.count("neu")) {
            std::cout << "- Writing Patran File..." << std::endl;
            std::cout
                << "-- Filename: "
                << (output_filename + ".neu").c_str()
                << std::endl;

            std::ofstream neu_file((output_filename + ".neu").c_str());
            neu_file << std::scientific;
            neu_file.precision(16);

            if(neu_file) {
                output_c3t3_to_patran(neu_file, c3t3);
            }
            
            if(!neu_file) {
                std::cout << "-- File operation failed!" << std::endl;
                std::cout << "--- Error: " << strerror(errno) << std::endl;
            }

            std::cout << "-- Done" << std::endl;
        }

        std::cout << "- Done!" << std::endl;

    } // if(generate_volume_mesh)



    // All done! //////////////////////////////////////////////////////////////
    return EXIT_SUCCESS;
}
