#ifndef COMPUTE_BOUNDING_SPHERE_H_
#define COMPUTE_BOUNDING_SPHERE_H_


// Minimum bounding sphere generation
#include <CGAL/Min_sphere_of_points_d_traits_3.h>
#include <CGAL/Min_sphere_of_spheres_d.h>

#include <cmath>


///////////////////////////////////////////////////////////////////////////////
// Compute Bounding Sphere_3                                                 //
///////////////////////////////////////////////////////////////////////////////
//
// Wrap bounding sphere calculation for list of points
// PointIterator thath should be convertable to Kernel::Point_3
//


// Convert Min_sphere_d to Sphere_3 ///////////////////////////////////////////
template<typename Kernel, typename Min_sphere_d>
typename Kernel::Sphere_3 min_sphere_to_sphere_3(
    Min_sphere_d& min_sphere_d
) {
    // Squared radius of the sphere
    typename Kernel::FT radius2 = std::pow(min_sphere_d.radius(), 2);

    // Center point of the sphere
    typename Min_sphere_d::Cartesian_const_iterator coord =
        min_sphere_d.center_cartesian_begin();

    // This could be denser, but less clear and implementation
    // dependant. So... it's fine?
    typename Kernel::FT x = *coord;
    coord++;
    typename Kernel::FT y = *coord;
    coord++;
    typename Kernel::FT z = *coord;


    return typename Kernel::Sphere_3(typename Kernel::Point_3(x,y,z), radius2);
}


// Generate Min_sphere_d from point iterator //////////////////////////////////
template<typename Kernel, typename PointIterator>
typename Kernel::Sphere_3 compute_bounding_sphere(
    PointIterator points_begin, PointIterator points_end
) {
    typedef
        CGAL::Min_sphere_of_points_d_traits_3<
            Kernel, typename Kernel::FT
        >
        Sphere_traits;
    typedef CGAL::Min_sphere_of_spheres_d<Sphere_traits> Min_sphere_d;


    // Generate the Sphere_d
    Min_sphere_d min_sphere_d(
        points_begin, points_end
    ); 

    return min_sphere_to_sphere_3<Kernel>( min_sphere_d );
}


#endif // COMPUTE_BOUNDING_SPHERE_H_
