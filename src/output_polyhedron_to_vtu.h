#ifndef OUTPUT_POLYHEDRON_TO_VTU_H_
#define OUTPUT_POLYHEDRON_TO_VTU_H_


#include <exception>
#include <map>


template<typename Stream, typename Polyhedron>
void output_polyhedron_to_vtu(
    Stream& polyhedron_vtu, const Polyhedron& polyhedron
) {

    typedef typename Polyhedron::Vertex_const_iterator PVit;
    typedef typename Polyhedron::Facet_const_iterator PFit;
    typedef
        typename Polyhedron::Halfedge_around_facet_const_circulator
        PHc;


    // The polyhedron must be triangular!
    if(!polyhedron.is_pure_triangle()) {
        throw std::logic_error(
            "output_polyhedron_vtu: Input polyhedron must be pure triangular."
        );
    }

    std::map<PVit, std::size_t> polyhedron_vertex_index;

    //Header
    polyhedron_vtu
        << "<?xml version=\"1.0\"?>\n"
        << "<VTKFile type=\"UnstructuredGrid\"  version=\"0.1\"  >\n"
        << "<UnstructuredGrid>\n"
        << "<Piece  "
            << "NumberOfPoints=\""
                << polyhedron.size_of_vertices()
            << "\" NumberOfCells=\""
                << polyhedron.size_of_facets()
            << "\">\n";

    // Output vertices and map iterator positions
    polyhedron_vtu
        << "<Points>\n"
        << "<DataArray "
            << "type=\"Float64\" "
            << "NumberOfComponents=\"3\" "
            << "format=\"ascii\""
        << ">";
    std::size_t max_pvit_index = 0;
    for(
        PVit pvit = polyhedron.vertices_begin();
        pvit != polyhedron.vertices_end();
        pvit++
    ) {
        polyhedron_vertex_index[pvit] = max_pvit_index;
        max_pvit_index++;

        typename Polyhedron::Point_3 p = pvit->point();

        polyhedron_vtu
            << p.x() << " " << p.y() << " " << p.z() << "  ";  
    }
    polyhedron_vtu
        << "</DataArray>\n"
        << "</Points>\n";

    // Output Cell data
    polyhedron_vtu << "<Cells>\n";

    polyhedron_vtu
        << "<DataArray "
            << "type=\"UInt32\" "
            << "Name=\"connectivity\" "
            << "format=\"ascii\">";
    for(
        PFit pfit = polyhedron.facets_begin();
        pfit != polyhedron.facets_end();
        pfit++
    ) {
        PHc phc = pfit->facet_begin();
        PHc phc_end = phc;

        do {
            polyhedron_vtu
                << polyhedron_vertex_index[phc->vertex()]
                << " ";
            phc++;
        } while(phc != phc_end);
        polyhedron_vtu << " ";
    }
    polyhedron_vtu << "</DataArray>\n";

    polyhedron_vtu
        << "<DataArray "
            << "type=\"UInt32\" "
            << "Name=\"offsets\" "
            << "format=\"ascii\">";
    for(
        std::size_t i=0;
        i<polyhedron.size_of_facets();
        i++
    ) {
        polyhedron_vtu << 3*(i+1) << " ";
    }
    polyhedron_vtu << "</DataArray>\n";

    polyhedron_vtu
        << "<DataArray "
            << "type=\"UInt32\" "
            << "Name=\"types\" "
            << "format=\"ascii\">";
    for(
        std::size_t i=0;
        i<polyhedron.size_of_facets();
        i++
    ) {
        // type 5 is a triangle!
        polyhedron_vtu << 5 << " ";
    }
    polyhedron_vtu << "</DataArray>\n";

    polyhedron_vtu
        << "</Cells>\n"
        << "</Piece>\n"
        << "</UnstructuredGrid>\n"
        << "</VTKFile>";
}

#endif // OUTPUT_POLYHEDRON_TO_VTU_H_
