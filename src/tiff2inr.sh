#!/usr/bin/env bash

#
# This script converts a multipage TIFF file to a 3D INR file.
#

if [[ $# -ne 5 ]]; then
    echo "Usage: $0 tiff_filename inr_filename voxel_size_x voxel_size_y voxel_size_z"
    exit 1
fi



#
# Gather information about the input TIFF file.
#
echo
echo Gathering .tiff file info...
echo

tiffFilename="$1"

inrFilename="$2"

# Get voxel sizes from input
voxelSizeX="$3"
voxelSizeY="$4"
voxelSizeZ="$5"

#
# Get voxel counts from TIFF image data
#

# Default values
voxelNumX=0
voxelNumY=0
voxelNumZ=0

# Use identify from ImageMagick to work out voxel counts
voxelNumX=$(identify -format "%w" "$tiffFilename[0]")
voxelNumY=$(identify -format "%h" "$tiffFilename[0]")
voxelNumZ=$(identify -format "%n" "$tiffFilename")

## Setting variables inside a while loop needs some
## fairly crappy syntax. WTF Bash?
#while_input() { identify -verbose "$tiffFilename"[0] 2>&1 ; }
#while read line; do
#
#    #Match identify error
#    if [[ $line == *identify:* ]]; then
#        echo Identify Error:
#        echo "$line" | sed 's/^\ *identify:/    /'
#        echo
#    fi
#
#    # Match dimensions for XxY dims
#    if [[ $line == *Geometry* ]]; then
#        geometry=`echo $line | sed 's/.*: \([[:digit:]]*x[[:digit:]]*\)+.*/\1/'`
#
#        voxelNumX=`echo $geometry | sed 's/x.*//'`
#        voxelNumY=`echo $geometry | sed 's/.*x//'`
#    fi
#
#    # Match number of images
#    if [[ $line == *images* ]]; then
#        voxelNumZ=`echo $line | sed 's/.*images=\([[:digit:]]*\).*/\1/'`
#    fi
#
#done <<EOW
#$(while_input)
#EOW

# Sanity check
if [[ $voxelNumX -eq 0 ]] || [[ $voxelNumY -eq 0 ]] || [[ $voxelNumZ -eq 0 ]];
then
    echo "Couldn't find voxel counts! Aborting."
    exit 1
fi

echo Voxel Geometry: "$voxelSizeX"x"$voxelSizeY"x"$voxelSizeZ"
echo Voxel Counts:   "$voxelNumX"x"$voxelNumY"x"$voxelNumZ"
echo


#
# Write INR header
# Format based on http://serdis.dis.ulpgc.es/~krissian/InrView1/IOformat.html
#
echo Writing .inr file header...
echo

cat > "$inrFilename" <<EOF
#INRIMAGE-4#{
XDIM=$voxelNumX
YDIM=$voxelNumY
ZDIM=$voxelNumZ
VDIM=1
VX=$voxelSizeX
VY=$voxelSizeY
VZ=$voxelSizeZ
TYPE=unsigned fixed
PIXSIZE=8 bits
SCALE=1
CPU=sun
EOF

# Header needs to be 256 characters long including the final newline

# Check how many characters there are so far
numChars=`wc -m < "$inrFilename"`

# Pad with '.' to 256 characters minus \n##}\n
for i in $(seq $numChars 250); do
    echo -n . >> "$inrFilename"
done
echo >> "$inrFilename"
echo '##}' >> "$inrFilename"


#
# Read TIFF files one by one by converting TIFF to 8 bit PGM
# and catting the binary data to the inr file.
#
echo Converting and writing data...
echo

# temporary PGM filename for data conversion
pgmFilename=$(mktemp --suffix=".pgm")

for n in $( seq 0 $(( $voxelNumZ - 1 )) ); do
    printf '\rProcessing slice %d of %d... ' "$(( $n + 1 ))" "$(( $voxelNumZ ))"
    convert -quiet "$tiffFilename"["$n"] -depth 8 "$pgmFilename"
    tail -n 1 "$pgmFilename" >> "$inrFilename"
done
echo
echo

# Remove temporary PGM file
rm "$pgmFilename"

echo 'Done!'
