#ifndef OUTPUT_IMAGE_3_TO_STL_H_
#define OUTPUT_IMAGE_3_TO_STL_H_

template<typename Stream, typename Image_3>
void output_image_3_to_stl(
    Stream& image_3_stl, const Image_3& image_3
) {
    // Output edge voxels to stl file
    image_3_stl << "solid particle\n";
    for(std::size_t k=0; k<image_3.zdim(); k++)
    for(std::size_t j=0; j<image_3.ydim(); j++)
    for(std::size_t i=0; i<image_3.xdim(); i++) {

        // Skip zero valued points
        if(image_3.value(i,j,k) == 0) continue;

        double vx = image_3.vx(), vy = image_3.vy(), vz = image_3.vz();
        double x = i*vx, y = j*vy, z = k*vz+0.001;
        
        // Check top and bottom
        if(
            i==0 || image_3.value(i-1,j,k) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y << " " << z << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z+vz << "\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }
        if(
            i==image_3.xdim()-1 || image_3.value(i+1,j,k) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }

        // Check left and right
        if(
            j==0 || image_3.value(i,j-1,k) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }
        if(
            j==image_3.ydim()-1 || image_3.value(i,j+1,k) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }

        // Check top and bottom
        if(
            k==0 || image_3.value(i,j,k-1) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y << " " << z << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }
        if(
            k==image_3.zdim()-1 || image_3.value(i,j,k+1) == 0
        ) {
            image_3_stl
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x << " " << y+vy << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n"
                << "facet normal 0 0 0\n"
                << "    outer loop\n"
                << "        vertex "
                    << x << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y << " " << z+vz << "\n"
                << "        vertex "
                    << x+vx << " " << y+vy << " " << z+vz << "\n"
                << "    endloop\n"
                << "endfacet\n";
        }

    }
    image_3_stl << "endsolid particle";
}

#endif // OUTPUT_IMAGE_3_TO_STL_H_
