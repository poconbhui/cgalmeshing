#ifndef OUTPUT_C3T3_TO_PATRAN_H_
#define OUTPUT_C3T3_TO_PATRAN_H_

#include <exception>
#include <algorithm>
#include <map>
#include <vector>
#include <cstdio>


///////////////////////////////////////////////////////////////////////////////
// Output Patran file                                                        //
///////////////////////////////////////////////////////////////////////////////
//
// It's worth noting that this could be completely incorrect.
// The Patran format defined some expected string formatting somewhere,
// but I couldn't find the reference again when I actually wrote this.
//
template<typename Stream, typename C3t3>
void output_c3t3_to_patran(
    Stream& neu_file, const C3t3& c3t3
) {
    typedef typename C3t3::Cell_iterator Cit;

    typedef typename C3t3::Triangulation Tr;
    typedef typename Tr::Vertex_handle Vertex_handle;
    typedef typename Tr::Point Point_3;
    typedef typename Tr::Finite_vertices_iterator FVit;

    const Tr& tr = c3t3.triangulation();


    std::map<Vertex_handle, std::size_t> vertex_to_index;

    // String buffer for output formatting
    const std::size_t n_strbuf = 1024;
    char strbuf[n_strbuf];

    // Common string format for Patran neutral card headers
    const char* i28i8 = "%02d%8d%8d%8d%8d%8d%8d%8d%8d\n";


    ///////////////////////////////////////////////////////////////////////////
    // Start Patran file.                                                    //
    ///////////////////////////////////////////////////////////////////////////
    // File format taken from                                                //
    // http://www.ansys.com/staticassets/ANSYS                               //
    //  /Initial%20Content%20Entry/General%20Articles%20-%20Products         //
    //  /ICEM%20CFD%20Interfaces/patran.htm                                  //
    //                                                                       //
    // and (more completely, especially for format strings)                  //
    //     Patran 2014.1 Reference Manual Part 1: Basic Functions,           //
    //     The Neutral File, p 892                                           //
    // found at                                                              //
    //     https://simcompanion.mscsoftware.com/infocenter/                  //
    //         index?page=content&id=DOC10756                                //
    //         &cat=2014.1_PATRAN_DOCS&actp=LIST                             //
    ///////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////
    // Packet 25: Title Card                                                 //
    ///////////////////////////////////////////////////////////////////////////
    // The title card packet contains the following:                         //
    //         25 ID IV KC                                                   //
    //         TITLE                                                         //
    //                                                                       //
    // where:  ID = 0 (not applicable)                                       //
    //         IV = 0 (not applicable                                        //
    //         KC = 1                                                        //
    //         TITLE = ICEM-PATRAN INTERFACE - Version ... -                 //
    //                                                                       //
    // format: (I2, 8I8)                                                     //
    //         (80A4)                                                        //
    ///////////////////////////////////////////////////////////////////////////

    // 25 ID IV KC
    // (I2, 8I8)
    snprintf(strbuf, n_strbuf, i28i8, 25, 0, 0, 1, 0, 0, 0, 0, 0);
    neu_file << strbuf;

    // TITLE
    // (80A4)
    snprintf(
        strbuf, n_strbuf, "%.320s\n",
        "Mesh generator by Padraig O Conbhui using the CGAL library."
    );
    neu_file << strbuf;



    ///////////////////////////////////////////////////////////////////////////
    // Packet 26: Summary Data                                               //
    ///////////////////////////////////////////////////////////////////////////
    // The summary data packet contains the following:                       //
    //         26 ID IV KC N1 N2 N3 N4 N5                                    //
    //         DATE TIME VERSION                                             //
    //                                                                       //
    // where:  ID = 0 (not applicable)                                       //
    //         IV = 0 (not applicable)                                       //
    //         KC = 1                                                        //
    //         N1 = number of nodes                                          //
    //         N2 = number of elements                                       //
    //         N3 = number of materials                                      //
    //         N4 = number of element properties                             //
    //         N5 = number of coordinate frames                              //
    //         DATE = dd-mm-yy                                               //
    //         TIME = hh:mm:ss                                               //
    //         VERSION = 2.5                                                 //
    //                                                                       //
    // format: (I2, 8I8)                                                     //
    //         (3A4, 2A4, 3A4)                                               //
    ///////////////////////////////////////////////////////////////////////////

    // TODO: Add number of materials (ie number of subdomains)

    // 26 ID IV KC N1 N2 N3 N4 N5
    // (I2, 8I8)
    snprintf(
        strbuf, n_strbuf, i28i8,
        26, 0, 0, 1,
        tr.number_of_vertices(), c3t3.number_of_cells_in_complex(),
        1, 0, 0
    );
    neu_file << strbuf;

    // DATE TIME VERSION
    // (3A4, 2A4, 3A4)
    snprintf(
        strbuf, n_strbuf, "%-12s%-8s%12s\n",
        "00/00/00", "00:00:00", "2.5"
    );
    neu_file << strbuf;



    ///////////////////////////////////////////////////////////////////////
    // Packet 01: Node Data                                              //
    ///////////////////////////////////////////////////////////////////////
    // The node data packet contains the following:                      //
    //         1 ID IV KC                                                //
    //         X Y Z                                                     //
    //         ICF GTYPE NDF CONFIG CID PSP                              //
    //                                                                   //
    // where:  ID = node ID                                              //
    //         IV = 0 (not applicable)                                   //
    //         KC = number of lines in data card = 2                     //
    //         X, Y, Z = X, Y, Z cartesian coordinate of the node        //
    //         ICF = 0 (unreferenced)                                    //
    //             Not sure about this one, but CUBIT uses 0, so ...     //
    //         GTYPE = G                                                 //
    //         NDF = 2 or 3 for 2D or 3D model respectively              //
    //         CONFIG = 0 (not applicable)                               //
    //         CID = 0 i.e. global cartesian coordinate system           //
    //         PSPC = 000000 (not used)                                  //
    //                                                                   //
    // format: (I2, 8I8)                                                 //
    //         (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)    //
    //         (I1, 1A1, I8, I8, I8, 2X, 6I1)                            //
    ///////////////////////////////////////////////////////////////////////

    std::size_t current_vertex_index = 1;
    for(
        FVit fvit = tr.finite_vertices_begin();
        fvit != tr.finite_vertices_end();
        fvit++, current_vertex_index++
    ) {

        vertex_to_index[fvit] = current_vertex_index;

        Point_3 p = fvit->point();

        // 1 ID IV KC
        // (I2, 8I8)
        snprintf(
            strbuf, n_strbuf, i28i8,
            1, vertex_to_index[fvit], 0, 2,
            0, 0, 0, 0, 0
        );
        neu_file << strbuf;

        // X Y Z
        // (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)
        snprintf(
            strbuf, n_strbuf, " % 15.8E % 15.8E % 15.8E\n",
            CGAL::to_double(p.x()),
            CGAL::to_double(p.y()),
            CGAL::to_double(p.z())
        );
        neu_file << strbuf;


        // ICF GTYPE NDF CONFIG CID PSP
        // (I1, 1A1, I8, I8, I8, 2X, 6I1)
        snprintf(
            strbuf, n_strbuf, "%1d%1c%8d%8d%8d  %1d%1d%1d%1d%1d%1d\n",
            0, 'G', 3, 0, 0,
            0, 0, 0, 0, 0, 0
        );
        neu_file << strbuf;
    }



    ///////////////////////////////////////////////////////////////////////
    // Packet 02: Element Data                                           //
    ///////////////////////////////////////////////////////////////////////
    // The element data packet contains the following:                   //
    //         2 ID IV KC N1 N2                                          //
    //         NODES CONFIG PID CEID q1 q2 q3                            //
    //         LNODES                                                    //
    //         ADATA                                                     //
    //                                                                   //
    // where:  ID = element ID                                           //
    //         IV = shape (2=bar, 3=tri, 4=quad,                         //
    //                     5=tet, 7=wedge, 8=hex, 9=pyra)                //
    //         KC = number of lines in data card                         //
    //         N1 = 0 (not used)                                         //
    //         N2 = 0 (not used)                                         //
    //         NODES = number of nodes in the element                    //
    //         CONFIG = 0 (not used)                                     //
    //         PID = element property ID                                 //
    //         CEID = 0 (not used)                                       //
    //         q1,q2,q3 = 0 (not used)                                   //
    //         LNODES = element corner nodes followed by                 //
    //                  additional nodes                                 //
    //         ADATA : not used                                          //
    //                                                                   //
    // format: (I2, 8I8)                                                 //
    //         (I8, I8, I8, I8, 3E16.9)                                  //
    //         (10I8)                                                    //
    ///////////////////////////////////////////////////////////////////////

    // We want to ensure the signed cell volumes we generate are all positive.
    // We also want them as close to ascending order as we can get.
    std::vector<std::pair<std::size_t, Vertex_handle> > cell_vertices(4);

    std::size_t current_cell_index = 1;
    for(
        Cit cit = c3t3.cells_in_complex_begin();
        cit != c3t3.cells_in_complex_end();
        cit++, current_cell_index++
    ) {
        // Get vertex index and handle pairs
        for(int i=0; i<4; i++) {
            cell_vertices[i].first  = vertex_to_index[cit->vertex(i)];
            cell_vertices[i].second = cit->vertex(i);
        }

        // Sort so vertex indices are in order.
        // (Pair sorts by first element first)
        // MERRILL does some index sorting on facets, but I'm
        // not sure if it will do a full sort ... ?
        std::sort(cell_vertices.begin(), cell_vertices.end());

        // Ensure volume is positive.
        // I'm not sure if MERRILL cares what direction the normals
        // actually are as long as they're consistent.
        // Having the same volume ensures the facet normals
        // point in consistent directions, eg always into or out of the
        // cell. It's in, I think, for positive volume ... ?
        double volume = CGAL::volume(
            cell_vertices[0].second->point(),
            cell_vertices[1].second->point(),
            cell_vertices[2].second->point(),
            cell_vertices[3].second->point()
        );
        if(volume < 0) {
            std::swap(cell_vertices[2], cell_vertices[3]);
        }

        // Recheck volume and complain if it hasn't changed!
        volume = CGAL::volume(
            cell_vertices[0].second->point(),
            cell_vertices[1].second->point(),
            cell_vertices[2].second->point(),
            cell_vertices[3].second->point()
        );
        if(volume < 0) {
            throw std::runtime_error("Volume Still Negative!");
        }


        // 2 ID IV KC N1 N2
        // (I2, 8I8)
        snprintf(
            strbuf, n_strbuf, i28i8,
            2, current_cell_index, 5, 2, 0, 0, 0, 0, 0
        );
        neu_file << strbuf;

        // NODES CONFIG PID CEID q1 q2 q3
        // (I8, I8, I8, I8, 3E16.9)
        snprintf(
            strbuf, n_strbuf, "%8d%8d%8lu%8d % 15.8E % 15.8E % 15.8E\n",
            4, 0,
            static_cast<unsigned long>(c3t3.subdomain_index(cit)), 0,
            0.0, 0.0, 0.0
        );
        neu_file << strbuf;

        // LNODES
        // (10I8) (well, 4I8 here)
        snprintf(
            strbuf, n_strbuf, "%8lu%8lu%8lu%8lu\n",
            // + 1 because CGAL is 0 indexed, but Patran is 1 indexed.
            static_cast<unsigned long>(cell_vertices[0].first),
            static_cast<unsigned long>(cell_vertices[1].first),
            static_cast<unsigned long>(cell_vertices[2].first),
            static_cast<unsigned long>(cell_vertices[3].first)
        );
        neu_file << strbuf;
    }

    // Packet 99: End of Neutral File
    snprintf(strbuf, n_strbuf, i28i8, 99, 0, 0, 1, 0, 0, 0, 0, 0);
    neu_file << strbuf;

    // Done outputting mesh ///////////////////////////////////////////////////
}


#endif // OUTPUT_C3T3_TO_PATRAN_H_
