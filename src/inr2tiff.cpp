#include <iostream>
#include <fstream>

#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <CGAL/Image_3.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;


int main(int argc, char* argv[]) {

    ///////////////////////////////////////////////////////////////////////////
    // Set commandline options                                               //
    ///////////////////////////////////////////////////////////////////////////

    std::string usage(
        "Usage: inr2tiff input output"
    );
    std::string description(
        "Convert an INR file to a TIFF file."
    );


    // Setup the options //////////////////////////////////////////////////////

    po::positional_options_description positional_options;
    positional_options.add("input",  1);
    positional_options.add("output", 1);


    po::options_description required_options("Required");
    required_options.add_options() (
        "input,i", po::value<std::string>()->required(),
        "The input INR file."
    ) (
        "output,o", po::value<std::string>()->required(),
        "The output TIFF file."
    );


    po::options_description help_options("Help");
    help_options.add_options() (
        "help,h", "Print this help message."
    );


    po::options_description cmdline_options;
    cmdline_options.add(required_options).add(help_options);


    // Parse the commandline arguments /////////////////////////////////////////

    po::variables_map args;

    try {
        po::store(
            po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .positional(positional_options)
                .run(),
            args
        );

        if(args.count("help")) {
            std::cout
                << std::endl
                << usage << std::endl << std::endl
                << description << std::endl
                << cmdline_options << std::endl;
            return 0;
        }

        po::notify(args);

    } catch(std::exception& e) {
        std::cout
            << std::endl
            << "Error: " << e.what() << std::endl << std::endl
            << usage << std::endl;
        return 1;
    }


    ///////////////////////////////////////////////////////////////////////////


    // Set filenames
    std::string input_inr_filename   = args["input"].as<std::string>();
    std::string output_tiff_filename = args["output"].as<std::string>();


    //
    // Read input image and determine some characteristics.
    //
    std::cout << "Processing input image..." << std::endl;

    // Input image
    CGAL::Image_3 image;
    image.read(input_inr_filename.c_str());

    // Get dimensions
    std::size_t xdim = image.xdim();
    std::size_t ydim = image.ydim();
    std::size_t zdim = image.zdim();

    // Find max pixel value
    std::size_t pix_max = std::numeric_limits<std::size_t>::min();
    for(std::size_t k=0; k<zdim; k++)
    for(std::size_t j=0; j<ydim; j++)
    for(std::size_t i=0; i<xdim; i++) {
        if(pix_max < image.value(i,j,k)) pix_max = image.value(i,j,k);
    }



    //
    // Output image to layers of pgm files.
    //
    std::cout << "Outputting PGM files..." << std::endl;


    // Temporary Output Image
    char tmp_img_filename_format[] = "%s/tmp_img_%04d.pgm";
    char tmp_img_filename_buf[256];
    std::ofstream tmp_img;

    // Get temporary directory for output.
    // (Pretty ugly!)
    char temp_dir[200] = "";

    // Open a pipe to the mktemp command.
    std::FILE* fp = popen("mktemp -d --tmpdir=$PWD inr2tiff-XXXXXXXXXX", "r");

    // Read the output of mktemp line by line.
    // We expect the output to be one \n terminated line with the
    // temporary directory name.
    // We loop over an unknown number of lines anyway.
    char fp_buf[200];
    while(fgets(fp_buf, 200, fp)) {

        // Make sure our string is definitely terminated.
        fp_buf[199] = '\0';
        std::cout << fp_buf << std::endl;

        // If we have a non-empty line, that should be out folder!
        if(strnlen(fp_buf, 200) > 1) {
            std::strncpy(temp_dir, fp_buf, 200);
            int len = strnlen(temp_dir, 200);
            if(temp_dir[len-1] == '\n') temp_dir[len-1] = '\0';
        }
    }
    int fp_exit_status = pclose(fp);
    // Check we got a temp folder
    if(fp_exit_status != 0 || strnlen(temp_dir, 200) <= 1) {
        std::cerr << "Unable to open temporary folder!" << std::endl;
        return 1;
    }


    // Loop through z layers, putting each layer in a different file
    std::cout << std::endl;
    for(std::size_t layer=0; layer < zdim; layer++) {
        //#warning debug break
        //if(layer > 5) break; // Test break early

        std::cout
            << "\r\033[K"
            << "-Writing File " << layer << " of " << zdim << "  "
            << std::flush;

        // Open PGM file for this layer
        std::snprintf(
            tmp_img_filename_buf, 255,
            tmp_img_filename_format, temp_dir, static_cast<int>(layer)
        );
        tmp_img.open(tmp_img_filename_buf);

        // Output PGM header
        tmp_img
            << "P2" << std::endl
            << xdim << " " << ydim << std::endl
            << pix_max << std::endl;

        // Output data
        for(std::size_t j=0; j<ydim; j++) {
            for(std::size_t i=0; i<xdim; i++) {
                tmp_img << image.value(i,j,layer) << " ";
            }
            tmp_img << "\n";
        }

        // Close PGM file for this layer
        if(tmp_img.is_open()) tmp_img.close();
    }
    std::cout << std::endl;


    //
    // Concat the PGM files into a TIFF file
    //
    std::cout << "Converting PGM images to TIFF file..." << std::endl;

    char convert_command_format[] = "convert %s/* %s";
    char convert_command_buf[256];
    std::snprintf(
        convert_command_buf, 255,
        convert_command_format, temp_dir, output_tiff_filename.c_str()
    );
    convert_command_buf[255] = '\0';
    std::cout << "Running command: " << convert_command_buf << std::endl;
    int convert_exit_value = std::system(convert_command_buf);
    if(convert_exit_value != 0) {
        std::cerr
            << "There was a problem generating the tiff file!"
            << std::endl;
        return 1;
    }


    //
    // Remove the temporary directory and files.
    //
    std::cout << "Cleaning up temporary files and folders..." << std::endl;

    char rm_command_format[] = "rm -r %s";
    char rm_command_buf[256];
    std::snprintf(
        rm_command_buf, 255,
        rm_command_format, temp_dir
    );
    std::cout << "Running command: " << rm_command_buf << std::endl;
    int rm_exit_value = std::system(rm_command_buf);
    if(rm_exit_value != 0) {
        std::cerr
            << "There was a problem removing the temporary directory "
            << "and temporary PGM files."
            << std::endl;
        return 1;
    }


    return 0;
}
