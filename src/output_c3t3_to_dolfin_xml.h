#ifndef OUTPUT_C3T3_TO_DOLFIN_XML_H_
#define OUTPUT_C3T3_TO_DOLFIN_XML_H_


//
// Output Dolfin Mesh and MeshFunction xml
//
template<typename Stream, typename C3t3>
void output_c3t3_to_dolfin_xml(
    Stream& c3t3_xml, const C3t3& c3t3
) {
    typedef typename C3t3::Cell_iterator Cit;

    typedef typename C3t3::Triangulation Tr;
    typedef typename Tr::Vertex_handle Vertex_handle;
    typedef typename Tr::Point Point_3;
    typedef typename Tr::Finite_vertices_iterator FVit;

    const Tr& tr = c3t3.triangulation();


    std::map<Vertex_handle, std::size_t> vertex_to_index;


    // Start Dolfin XML file.
    c3t3_xml << "<dolfin>\n";


    ///////////////////////////////////////////////////////////////////////////
    // Output Mesh File                                                      //
    ///////////////////////////////////////////////////////////////////////////
    //
    // Techniques stolen shamelessly from include/CGAL/IO/File_medit.h
    //

    c3t3_xml << "  <mesh celltype=\"tetrahedron\" dim=\"3\">\n";

    // Output Mesh Vertices
    c3t3_xml
        << "    "
        << "<vertices "
            <<  "size=\"" << tr.number_of_vertices() << "\""
        << ">\n";
    std::size_t current_vertex_index = 0;
    for(
        FVit fvit = tr.finite_vertices_begin();
        fvit != tr.finite_vertices_end();
        fvit++, current_vertex_index++
    ) {
        vertex_to_index[fvit] = current_vertex_index;

        Point_3 p = fvit->point();

        c3t3_xml
            << "      <vertex "
                << "index=\"" << vertex_to_index[fvit] << "\" "
                << "x=\"" << CGAL::to_double(p.x()) << "\" "
                << "y=\"" << CGAL::to_double(p.y()) << "\" "
                << "z=\"" << CGAL::to_double(p.z()) << "\" "
            << "/>\n";
    }
    c3t3_xml << "    </vertices>\n";

    // Output Mesh Cells
    c3t3_xml
        << "    <cells "
            << "size=\"" << c3t3.number_of_cells_in_complex() << "\""
        << ">\n";
    std::vector<std::pair<std::size_t, Vertex_handle> > cell_vertices(4);
    std::size_t current_cell_index = 0;
    for(
        Cit cit = c3t3.cells_in_complex_begin();
        cit != c3t3.cells_in_complex_end();
        cit++, current_cell_index++
    ) {
        // Get cell vertices and sort list by vertex index
        for(std::size_t i=0; i<4; i++) {
            cell_vertices[i].first  = vertex_to_index[cit->vertex(i)];
            cell_vertices[i].second = cit->vertex(i);
        }
        std::sort(cell_vertices.begin(), cell_vertices.end());

        // Ensure volume is positive.
        double volume = CGAL::volume(
            cell_vertices[0].second->point(),
            cell_vertices[1].second->point(),
            cell_vertices[2].second->point(),
            cell_vertices[3].second->point()
        );
        if(volume < 0) std::swap(cell_vertices[2], cell_vertices[3]);

        // Recheck volume and complain if it hasn't changed!
        volume = CGAL::volume(
            cell_vertices[0].second->point(),
            cell_vertices[1].second->point(),
            cell_vertices[2].second->point(),
            cell_vertices[3].second->point()
        );
        if(volume < 0) throw std::runtime_error("Volume Still Negative!");


        c3t3_xml
            << "      <tetrahedron "
                << "index=\"" << current_cell_index << "\" ";
        for(int i=0; i<4; i++) {
            c3t3_xml
                << "v" << i << "=\""
                    << cell_vertices[i].first
                << "\" ";
        }
        c3t3_xml << "/>\n";
    }
    c3t3_xml << "    </cells>\n";
    c3t3_xml << "  </mesh>\n";

    // Done outputting mesh ///////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////
    // Output Mesh Function to file                                          //
    ///////////////////////////////////////////////////////////////////////////

    c3t3_xml << "  <mesh_function>\n";

    // Output Mesh Cell Domain Values
    c3t3_xml
        << "    <mesh_value_collection "
            << "name=\"particle id\" "
            << "type=\"uint\" "
            << "dim=\"3\" "
            << "size=\"" << c3t3.number_of_cells_in_complex() << "\""
        << ">\n";
    current_cell_index = 0;
    for(
        Cit cit = c3t3.cells_in_complex_begin();
        cit != c3t3.cells_in_complex_end();
        cit++, current_cell_index++
    ) {
        c3t3_xml
            << "      <value "
                << "cell_index=\"" << current_cell_index << "\" "
                << "local_entity=\"0\" "
                << "value=\"" << c3t3.subdomain_index(cit) << "\" "
            << "/>\n";
    }
    c3t3_xml << "    </mesh_value_collection>\n";
    c3t3_xml << "  </mesh_function>\n";

    // Done outputting mesh function //////////////////////////////////////////


    // End Dolfin XML file
    c3t3_xml << "</dolfin>";
}


#endif // OUTPUT_C3T3_TO_DOLFIN_XML_H_
