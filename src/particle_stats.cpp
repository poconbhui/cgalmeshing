#define  _USE_MATH_DEFINES

#include <iostream>

#include <CGAL/Image_3.h>
#include <boost/program_options.hpp>

// Useful abbreviation ... ?
// Just following the examples...
namespace po = boost::program_options;

#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>


// Min Ellipse includes
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Approximate_min_ellipsoid_d.h>
#include <CGAL/Approximate_min_ellipsoid_d_traits_3.h>



// Setup Min ellipsoid calculations
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef
    CGAL::Approximate_min_ellipsoid_d_traits_3<Kernel, CGAL::MP_Float>
    MinEllipsoidTraits;
typedef CGAL::Approximate_min_ellipsoid_d<MinEllipsoidTraits> MinEllipsoid;

typedef MinEllipsoidTraits::Point Point;



// A structure for finding and storing info for a particle
struct ParticleInfo {
    std::size_t imax;
    std::size_t jmax;
    std::size_t kmax;

    std::size_t imin;
    std::size_t jmin;
    std::size_t kmin;

    double iavg;
    double javg;
    double kavg;

    std::size_t volume;

    // Store faces separately because vx,vy,vz can be different.
    std::size_t surface_area_xy;
    std::size_t surface_area_yz;
    std::size_t surface_area_xz;

    typedef std::vector<Point> Points;
    Points outer_points;

    ParticleInfo():
        imax(std::numeric_limits<std::size_t>::min()),
        jmax(std::numeric_limits<std::size_t>::min()),
        kmax(std::numeric_limits<std::size_t>::min()),

        imin(std::numeric_limits<std::size_t>::max()),
        jmin(std::numeric_limits<std::size_t>::max()),
        kmin(std::numeric_limits<std::size_t>::max()),

        iavg(0.0),
        javg(0.0),
        kavg(0.0),

        volume(0),

        surface_area_xy(0),
        surface_area_yz(0),
        surface_area_xz(0)
    {}
};



// Set unput vectors to accept exactly 3 arguments
template<typename T>
class fixed_width_typed_value: public po::typed_value<T> {
    typedef typename po::typed_value<T> Base;
public:
    fixed_width_typed_value(std::size_t width):
        Base(0),
        width(width)
    {
        Base::multitoken();
    }

    virtual unsigned min_tokens() const { return width; }
    virtual unsigned max_tokens() const { return width; }

    std::size_t width;
};


int main(int argc, char* argv[]) {

    ///////////////////////////////////////////////////////////////////////////
    // Set commandline options                                               //
    ///////////////////////////////////////////////////////////////////////////

    std::string usage(
        "Usage: [--target_xyz x y z | --target_ijk i j k | --target_label l] "
        "file"
    );

    std::string description(
        "Display some statistics for the labelled particles in an INR file."
    );


    // Setup the options //////////////////////////////////////////////////////

    po::positional_options_description positional_options;
    positional_options.add("file", 1);


    po::options_description required_options("Required");

    required_options.add_options() (
        "file", po::value<std::string>()->required(),
        "A .inr file with particles individually labelled."
    );


    po::options_description optional_options("Optional");

    optional_options.add_options() (
        "target_xyz,x",
        new fixed_width_typed_value< std::vector<double> >(3),
        "Print stats only for the particle which includes the point xyz."
    ) (
        "target_ijk,i",
        new fixed_width_typed_value< std::vector<std::size_t> >(3),
        "Print stats only for the particle which includes voxel ijk."
    ) (
        "target_label,l", po::value<std::size_t>(),
        "Print stats only for the particle with the given label."
    ) (
        "min_ellipsoid,e",
        "Compute approximate minimum bounding ellipsoid for each"
        " printed particle."
    ) (
        "surface_area,s",
        "Compute particle surface area"
    ) (
        "include_zero,z",
        "Include label 0 in stats. (Usually ignored as empty space label)"
    );


    po::options_description help_options("Help");
    help_options.add_options() (
        "help,h", "Prints a help message."
    );


    po::options_description cmdline_options;
    cmdline_options
        .add(required_options)
        .add(optional_options)
        .add(help_options);


    // Parse the commandline arguments /////////////////////////////////////////

    po::variables_map args;

    try {
        po::store(
            po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .positional(positional_options)
                .run(),
            args
        );


        // Output help
        if(args.count("help")) {
            std::cout
                << std::endl
                << usage << std::endl << std::endl
                << cmdline_options << std::endl;
            return 0;
        }


        po::notify(args);

    } catch(std::exception &e) {
        std::cout
            << std::endl
            << "Error: " << e.what() << std::endl << std::endl
            << usage << std::endl;
        return 1;
    }


    ///////////////////////////////////////////////////////////////////////////


    const char* input_filename = args["file"].as<std::string>().c_str();


    //
    // Process targets
    //

    // Target XYZ
    bool is_targeting_xyz = false;
    std::vector<double> target_xyz(3);

    if(args.count("target_xyz")) {
        is_targeting_xyz = true;
        target_xyz = args["target_xyz"].as< std::vector<double> >();
    }


    // Target IJK
    bool is_targeting_ijk = false;
    std::vector<std::size_t> target_ijk(3);

    if(args.count("target_ijk")) {
        is_targeting_ijk = true;
        target_ijk = args["target_ijk"].as< std::vector<std::size_t> >();
    }


    // Target Label
    bool is_targeting_label = false;
    std::size_t target_label = 0;

    if(args.count("target_label")) {
        is_targeting_label = true;
        target_label = args["target_label"].as<std::size_t>();
    }


    // Any targets set?
    bool is_targeting =
        is_targeting_xyz || is_targeting_ijk || is_targeting_label;



    // Should we compute a bounding elipsoid?
    bool compute_ellipsoid = args.count("min_ellipsoid");



    // Should we compute the surface area?
    bool compute_surface_area = args.count("surface_area");



    // Should we include stats for label zero?
    bool include_zero = args.count("include_zero");


    ///////////////////////////////////////////////////////////////////////////


    // Set cout precision
    std::cout.setf(std::ios::scientific, std::ios::floatfield);
    std::cout.precision(16);

    typedef std::map<std::size_t, ParticleInfo> ParticleInfoMap;
    ParticleInfoMap particle_info_map;

    CGAL::Image_3 image;
    image.read(input_filename);

    for(std::size_t k = 0; k < image.zdim(); k++)
    for(std::size_t j = 0; j < image.ydim(); j++)
    for(std::size_t i = 0; i < image.xdim(); i++) {
        std::size_t label = image.value(i,j,k);

        // Skip zero unless requested
        if(!include_zero && label == 0) {
            continue;
        }

        ParticleInfo& particle_info = particle_info_map[label];

        // Find bounding box
        if(i > particle_info.imax) particle_info.imax = i;
        if(j > particle_info.jmax) particle_info.jmax = j;
        if(k > particle_info.kmax) particle_info.kmax = k;

        if(i < particle_info.imin) particle_info.imin = i;
        if(j < particle_info.jmin) particle_info.jmin = j;
        if(k < particle_info.kmin) particle_info.kmin = k;


        particle_info.iavg += i;
        particle_info.javg += j;
        particle_info.kavg += k;


        // Find volume
        particle_info.volume++;


        if(compute_ellipsoid) {
            // Find all outer corner points.
            // If the voxel is on the outside of the particle, add
            // the corners.
            double x0 = i*image.vx();
            double y0 = j*image.vy();
            double z0 = k*image.vz();

            double x1 = (i+1)*image.vx();
            double y1 = (j+1)*image.vy();
            double z1 = (k+1)*image.vz();

            // We need to check if any voxels including our point ijk are
            // different than our own. If they are, add our point.
            if(
                i==0 || j==0 || k==0
                || image.value(i-1,j,k) != label
                || image.value(i,j-1,k) != label
                || image.value(i,j,k-1) != label
                || image.value(i-1,j-1,k) != label
                || image.value(i-1,j,k-1) != label
                || image.value(i,j-1,k-1) != label
                || image.value(i-1,j-1,k-1) != label
            ) {
                particle_info.outer_points.push_back(Point(x0, y0, z0));
            }

            // We need to check if the voxels that would checking our other
            // corner points are the same as this voxel. If not, add those
            // points.
            if(i==image.xdim()-1 || image.value(i+1,j,k) != label) {
                particle_info.outer_points.push_back(Point(x1, y0, z0));
            }
            if(j==image.ydim()-1 || image.value(i,j+1,k) != label) {
                particle_info.outer_points.push_back(Point(x0, y1, z0));
            }
            if(k==image.zdim()-1 || image.value(i,j,k+1) != label) {
                particle_info.outer_points.push_back(Point(x0, y1, z0));
            }
            if(
                i==image.xdim()-1 || j==image.ydim()-1
                || image.value(i+1,j+1,k) != label
            ) {
                particle_info.outer_points.push_back(Point(x1, y1, z0));
            }
            if(
                i==image.xdim()-1 || k==image.zdim()-1
                || image.value(i+1,j,k+1) != label
            ) {
                particle_info.outer_points.push_back(Point(x1, y0, z1));
            }
            if(
                j==image.ydim()-1 || k==image.zdim()-1
                || image.value(i,j+1,k+1) != label
            ) {
                particle_info.outer_points.push_back(Point(x0, y1, z1));
            }
            if(
                i==image.xdim()-1 || j==image.ydim()-1 || k==image.zdim()-1
                || image.value(i+1,j+1,k+1) != label
            ) {
                particle_info.outer_points.push_back(Point(x1, y1, z1));
            }
        }


        if(compute_surface_area) {
            if(i==0 || image.value(i-1,j,k) != label) {
                particle_info.surface_area_yz++;
            }
            if(i==image.xdim()-1 || image.value(i+1,j,k) != label) {
                particle_info.surface_area_yz++;
            }

            if(j==0 || image.value(i,j-1,k) != label) {
                particle_info.surface_area_xz++;
            }
            if(j==image.ydim()-1 || image.value(i,j+1,k) != label) {
                particle_info.surface_area_xz++;
            }

            if(k==0 || image.value(i,j,k-1) != label) {
                particle_info.surface_area_xy++;
            }
            if(k==image.zdim()-1 || image.value(i,j,k+1) != label) {
                particle_info.surface_area_xy++;
            }
        }
    }

    // Fix averages
    for(
        ParticleInfoMap::iterator pim_it = particle_info_map.begin();
        pim_it != particle_info_map.end();
        pim_it++
    ) {
        pim_it->second.iavg /= pim_it->second.volume;
        pim_it->second.javg /= pim_it->second.volume;
        pim_it->second.kavg /= pim_it->second.volume;
    }


    for(
        ParticleInfoMap::iterator pim_it = particle_info_map.begin();
        pim_it != particle_info_map.end();
        pim_it++
    ) {
        std::size_t label = pim_it->first;
        ParticleInfo& particle_info = pim_it->second;

        std::size_t xlen = (particle_info.imax + 1) - particle_info.imin;
        std::size_t ylen = (particle_info.jmax + 1) - particle_info.jmin;
        std::size_t zlen = (particle_info.kmax + 1) - particle_info.kmin;


        // Check if targeting xyz and if outputting
        bool is_xyz_target =
            (particle_info.imin-1)*image.vx() <= target_xyz[0]
            && (particle_info.imax+1)*image.vx() >= target_xyz[0]
            && (particle_info.jmin-1)*image.vy() <= target_xyz[1]
            && (particle_info.jmax+1)*image.vy() >= target_xyz[1]
            && (particle_info.kmin-1)*image.vz() <= target_xyz[2]
            && (particle_info.kmax+1)*image.vz() >= target_xyz[2];

        // Check if targeting ijk and if outputting
        bool is_ijk_target = 
            (particle_info.imin-1) <= target_ijk[0]
            && (particle_info.imax+1) >= target_ijk[0]
            && (particle_info.jmin-1) <= target_ijk[1]
            && (particle_info.jmax+1) >= target_ijk[1]
            && (particle_info.kmin-1) <= target_ijk[2]
            && (particle_info.kmax+1) >= target_ijk[2];

        // Check if targeting label and if outputting
        bool is_label_target = (label == target_label);


        // Output stats if requested
        if(
            (is_targeting_xyz && is_xyz_target)
            || (is_targeting_ijk && is_ijk_target)
            || (is_targeting_label && is_label_target)
            || !is_targeting
        ) {

            // All the possible particle infos

            // std::size_t label = label;

            std::size_t imax = 0,   jmax = 0,   kmax = 0;
            std::size_t imin = 0,   jmin = 0,   kmin = 0;

            double      xmax = 0.0, ymax = 0.0, zmax = 0.0;
            double      xmin = 0.0, ymin = 0.0, zmin = 0.0;

            double x_center = 0.0, y_center = 0.0, z_center = 0.0;

            std::size_t n_voxels = 0;
            double      volume   = 0.0;

            const std::size_t num_ellipsoid_axes = 3;
            std::vector<double> ellipsoid_axis_lengths(num_ellipsoid_axes);
            std::vector< std::vector<double> > ellipsoid_axes(
                num_ellipsoid_axes
            );
            for(std::size_t i=0; i<num_ellipsoid_axes; i++) {
                ellipsoid_axes[i].resize(num_ellipsoid_axes);
            }

            std::size_t n_voxel_surfaces_xy = 0;
            std::size_t n_voxel_surfaces_yz = 0;
            std::size_t n_voxel_surfaces_xz = 0;

            double surface_area = 0.0;


            // Set voxel maxs and mins
            imax = particle_info.imax + 1;
            jmax = particle_info.jmax + 1;
            kmax = particle_info.kmax + 1;

            imin = particle_info.imin;
            jmin = particle_info.jmin;
            kmin = particle_info.kmin;

            // Set unit maxs and mins
            xmax = imax*image.vx();
            ymax = jmax*image.vy();
            zmax = kmax*image.vz();

            xmin = imin*image.vx();
            ymin = jmin*image.vy();
            zmin = kmin*image.vz();

            // Set unit centers
            x_center = particle_info.iavg*image.vx();
            y_center = particle_info.javg*image.vy();
            z_center = particle_info.kavg*image.vz();

            // Set volumes
            n_voxels = particle_info.volume;
            volume = n_voxels*image.vx()*image.vy()*image.vz();

            // Set ellipsoid axes
            if(compute_ellipsoid) {
                double eps = 0.01;
                MinEllipsoid min_ellipsoid(
                    eps,
                    particle_info.outer_points.begin(),
                    particle_info.outer_points.end()
                );

                if(min_ellipsoid.is_full_dimensional()) {

                    typedef MinEllipsoid::Axes_lengths_iterator MEALit;
                    std::size_t al_i = 0;
                    for(
                        MEALit meal_it = min_ellipsoid.axes_lengths_begin();
                        meal_it != min_ellipsoid.axes_lengths_end();
                        ++meal_it, ++al_i
                    ) {
                        ellipsoid_axis_lengths[al_i] = *meal_it;
                    }

                    typedef
                        MinEllipsoid::Axes_direction_coordinate_iterator
                        MEADit;
                    for(std::size_t i=0; i<num_ellipsoid_axes; i++) {

                        std::size_t ad_i = 0;
                        for(
                            MEADit mead_it = min_ellipsoid
                                .axis_direction_cartesian_begin(i);
                            mead_it != min_ellipsoid
                                .axis_direction_cartesian_end(i);
                            ++mead_it, ++ad_i
                        ) {
                            ellipsoid_axes[i][ad_i] = *mead_it;
                        }

                    }

                    // Sort in Major, Intermediate and Minor order.
                    for(std::size_t i=0; i<num_ellipsoid_axes; i++)
                    for(std::size_t j=i+1; j<num_ellipsoid_axes; j++) {
                        if(
                            ellipsoid_axis_lengths[i]
                                < ellipsoid_axis_lengths[j]
                        ) {
                            std::swap(
                                ellipsoid_axis_lengths[i],
                                ellipsoid_axis_lengths[j]
                            );

                            std::swap(ellipsoid_axes[i], ellipsoid_axes[j]);
                        }
                    }
                }
            }

            if(compute_surface_area) {
                n_voxel_surfaces_xy = particle_info.surface_area_xy;
                n_voxel_surfaces_yz = particle_info.surface_area_yz;
                n_voxel_surfaces_xz = particle_info.surface_area_xz;

                surface_area = (
                    n_voxel_surfaces_xy*image.vx()*image.vy()
                    + n_voxel_surfaces_yz*image.vy()*image.vz()
                    + n_voxel_surfaces_xz*image.vx()*image.vz()
                );
            }



            bool old_output = true;
            if(old_output == true) {

                std::cout << std::endl;
                std::cout << "Label " << label << ":" << std::endl;

                std::cout << "-" << std::endl;
                std::cout << "- Voxels:" << std::endl;
                std::cout
                    << "-- Maxs: ("
                        << (particle_info.imax + 1) << ", "
                        << (particle_info.jmax + 1) << ", "
                        << (particle_info.kmax + 1)
                    << ")"
                    << std::endl;
                std::cout
                    << "-- Mins: ("
                        << particle_info.imin << ", "
                        << particle_info.jmin << ", "
                        << particle_info.kmin
                    << ")"
                    << std::endl;
                std::cout
                    << "-- Width: ("<< xlen <<", "<< ylen <<", "<< zlen <<")"
                    << std::endl;
                std::cout << "-- Volume: " << particle_info.volume << std::endl;

                std::cout << "-" << std::endl;
                std::cout << "- Units:" << std::endl;
                std::cout
                    << "-- Maxs: ("
                        << (particle_info.imax + 1)*image.vx() << ", "
                        << (particle_info.jmax + 1)*image.vy() << ", "
                        << (particle_info.kmax + 1)*image.vz()
                    << ")"
                    << std::endl;
                std::cout
                    << "-- Mins: ("
                        << particle_info.imin*image.vx() << ", "
                        << particle_info.jmin*image.vy() << ", "
                        << particle_info.kmin*image.vz()
                    << ")"
                    << std::endl;
                std::cout
                    << "-- Width: ("
                        << xlen*image.vx() <<", "
                        << ylen*image.vy() <<", "
                        << zlen*image.vz()
                    << ")"
                    << std::endl;
                std::cout
                    << "-- Volume: "
                    << particle_info.volume*image.vx()*image.vy()*image.vz()
                    << std::endl;


                if(compute_ellipsoid) {
                    std::cout << "-" << std::endl;
                    std::cout << "- Bounding Ellipsoid (Units):" << std::endl;

                    double eps = 0.01;
                    MinEllipsoid min_ellipsoid(
                        eps,
                        particle_info.outer_points.begin(),
                        particle_info.outer_points.end()
                    );


                    if(!min_ellipsoid.is_full_dimensional()) {
                        std::cout
                            << "-- Error: Not full dimensional!" << std::endl;
                    }


                    // Output Center
                    std::vector<double> min_ellipsoid_center;

                    typedef MinEllipsoid::Center_coordinate_iterator MECit;
                    for(
                        MECit mec_it = min_ellipsoid.center_cartesian_begin();
                        mec_it != min_ellipsoid.center_cartesian_end();
                        ++mec_it
                    ) {
                        min_ellipsoid_center.push_back(*mec_it);
                    }
                    if(min_ellipsoid_center.size() != 3) {
                        std::cout << "-- Error: Center not 3D!" << std::endl;
                    }

                    std::cout << "-- Center (";
                    for(std::size_t i=0; i<min_ellipsoid_center.size(); i++) {
                        std::cout << min_ellipsoid_center[i];
                        if(i!= min_ellipsoid_center.size()-1) {
                            std::cout << ", ";
                        }
                    }
                    std::cout << ")" << std::endl;


                    // Output Axes
                    std::vector<double> axis_lengths;
                    std::vector< std::vector<double> > axes;

                    typedef MinEllipsoid::Axes_lengths_iterator MEALit;
                    for(
                        MEALit meal_it = min_ellipsoid.axes_lengths_begin();
                        meal_it != min_ellipsoid.axes_lengths_end();
                        ++meal_it
                    ) {
                        axis_lengths.push_back(*meal_it);
                    }

                    if(axis_lengths.size() != 3) {
                        std::cout << "-- Error: Didn't find 3 Axes!" << std::endl;
                    }


                    axes.resize(axis_lengths.size());
                    typedef
                        MinEllipsoid::Axes_direction_coordinate_iterator
                        MEADit;
                    for(std::size_t i=0; i<axes.size(); i++) {
                        std::vector<double>& axis = axes[i];

                        for(
                            MEADit mead_it
                                = min_ellipsoid.axis_direction_cartesian_begin(i);
                            mead_it
                                != min_ellipsoid.axis_direction_cartesian_end(i);
                            ++mead_it
                        ) {
                            axis.push_back(*mead_it);
                        }

                        if(axis.size() != 3) {
                            std::cout
                                << "-- Error: Axis " << i << " not 3D!"
                                << std::endl;
                        }
                    }


                    std::cout << "-- Axes:" << std::endl;
                    for(std::size_t i=0; i<axis_lengths.size(); i++) {
                        std::vector<double>& axis = axes[i];

                        std::cout << "--- " << axis_lengths[i] << " (";
                        for(std::size_t i=0; i<axis.size(); i++) {
                            std::cout << axis[i];
                            if(i != axis.size()-1) {
                                std::cout << ", ";
                            }
                        }
                        std::cout << ")" << std::endl;
                    }
                }


                if(compute_surface_area) {
                    std::cout << "- Surface Area (Voxel surfaces):" << std::endl;
                    std::cout
                        << "-- XY: " << particle_info.surface_area_xy
                        << std::endl
                        << "-- YZ: " << particle_info.surface_area_yz
                        << std::endl
                        << "-- XZ: " << particle_info.surface_area_xz
                        << std::endl;
                    std::cout
                        << "- Surface Area (Units^2): "
                        << (
                            particle_info.surface_area_xy*image.vx()*image.vy()
                            + particle_info.surface_area_yz*image.vy()*image.vz()
                            + particle_info.surface_area_xz*image.vx()*image.vz()
                        )
                        << std::endl;
                }

            } else if(old_output == false) {

                static bool first_iteration = true;
                if(first_iteration) {
                    std::cout <<
                        "# front, particle_id, "
                        "particle_center_x, particle_center_y, particle_center_z, "
                        "particle_surface_area, particle_volume, ellipsoid_volume, "
                        "ellipsoid_major_radius, ellipsoid_intermediate_radius, "
                        "ellipsoid_minor_radius, "
                        "VxMj, VxIn, VxMn, VyMj, VyIn, VyMn, VzMj, VzIn, VzMn"
                        << std::endl;
                    first_iteration = false;
                }

                char buf[1024];
                snprintf(buf, 1024,
                    "front, "
                    "%lu, "
                    // x y z
                    "%e, %e, %e, "
                    // SA, Encl Vol, Ellipsoid Vol
                    "%e, %e, %e, "
                    // Maj radius, Int. radius, Min radius
                    "%e, %e, %e, "
                    // VxMj VxIn VxMn
                    "%e, %e, %e, "
                    // VyMj VyIn VyMn
                    "%e, %e, %e, "
                    // VzMj VzIn VzMn
                    "%e, %e, %e\n",

                    static_cast<unsigned long>(label),

                    x_center, y_center, z_center,

                    surface_area, volume,
                    (
                        4.0/3.0*M_PI
                        *(ellipsoid_axis_lengths[0]/2.0)
                        *(ellipsoid_axis_lengths[1]/2.0)
                        *(ellipsoid_axis_lengths[2]/2.0)
                    ),

                    ellipsoid_axis_lengths[0],
                    ellipsoid_axis_lengths[1],
                    ellipsoid_axis_lengths[2],

                    ellipsoid_axes[0][0],
                    ellipsoid_axes[1][0],
                    ellipsoid_axes[2][0],

                    ellipsoid_axes[0][1],
                    ellipsoid_axes[1][1],
                    ellipsoid_axes[2][1],

                    ellipsoid_axes[0][2],
                    ellipsoid_axes[1][2],
                    ellipsoid_axes[2][2]
                );

                std::cout << buf << std::flush;
            } // else if(old_output)

        }

    }


    return 0;
}
