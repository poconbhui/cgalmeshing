#ifndef _INR_IMAGE_WRITER_H_
#define _INR_IMAGE_WRITER_H_


#include <iostream>
#include <sstream>
#include <fstream>

#include <boost/endian/conversion.hpp>

#include <climits>


// A helper class for writing Inr files with a given type from an
// iterator of a different type.
class InrImageWriter {
public:

    // The number of voxels in each direction.
    std::size_t xdim;
    std::size_t ydim;
    std::size_t zdim;

    // The voxel dimensions.
    double vx;
    double vy;
    double vz;

    // The size of the data for each voxel.
    std::size_t vdim;


    InrImageWriter(
        std::size_t xdim, std::size_t ydim, std::size_t zdim,
        double vx, double vy, double vz,
        std::size_t vdim
    ):
        xdim(xdim), ydim(ydim), zdim(zdim),
        vx(vx), vy(vy), vz(vz),
        vdim(vdim)
    {}


    // Write data from the given iterator out to the stream s casting
    // the data to the OutputType
    template<class OutputType, class IteratorType, class StreamType>
    void write_as(
        const IteratorType& data_begin, const IteratorType& data_end,
        StreamType& s
    ) const {
        // Output header to stream.
        s << inr_header<OutputType>() << std::flush;
        write_data_with_type<OutputType>(data_begin, data_end, s);
    }


    // Generate an inr header using the class information
    // and setting the pixsize to an appropriate size for the OutputType.
    template<class OutputType>
    std::string inr_header() const {

        // Generate the text header
        // Format based on
        // http://serdis.dis.ulpgc.es/~krissian/InrView1/IOformat.html
        std::stringstream ss_inr_header;
        ss_inr_header
            << "#INRIMAGE-4#{\n"
            << "XDIM=" << xdim << "\n"
            << "YDIM=" << ydim << "\n"
            << "ZDIM=" << zdim << "\n"
            << "VDIM=" << vdim << "\n"
            << "VX=" << vx << "\n"
            << "VY=" << vy << "\n"
            << "VZ=" << vz << "\n"
            << "TYPE=unsigned fixed\n"
            << "PIXSIZE=" << CHAR_BIT*sizeof(OutputType) << " bits\n"
            << "SCALE=1\n";

        // Add CPU=endianness
        // The boost::endian solution isn't binary portable,
        // as in this binary won't work properly if copy/pasted
        // to a big-endian system. It should work if compiled
        // on a big endian one though.
        ss_inr_header << "CPU=sun\n"; // Big-Endian output

        // Header should be 256 characters long.
        // Add a line of '.' to fill the extra space.
        std::size_t padding_needed = 251 - ss_inr_header.str().length();
        ss_inr_header << std::string(padding_needed, '.') << "\n";
        ss_inr_header << "##}\n";

        std::cout
            << "-Header For .inr File:\n"
            << ss_inr_header.str()
            << std::endl;

        return ss_inr_header.str();
    }


    // Write the raw data to the stream from an iterator, converting
    // the iterator data to the OutputType.
    template<class OutputType, class IteratorType, class StreamType>
    void write_data_with_type(
        IteratorType it, const IteratorType& it_end, StreamType& s
    ) const {
        while(it != it_end) {
            OutputType v = *it;
            boost::endian::native_to_big_inplace(v);
            s.write(
                reinterpret_cast<const char*>(&v), sizeof(OutputType)
            );

            it++;
        }
    }
};




#endif // _INR_IMAGE_WRITER_H_
