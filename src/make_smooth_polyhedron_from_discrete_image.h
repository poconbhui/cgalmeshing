#ifndef MAKE_SMOOTH_POLYHEDRON_FROM_DISCRETE_IMAGE_H_
#define MAKE_SMOOTH_POLYHEDRON_FROM_DISCRETE_IMAGE_H_

#include <iostream>
#include <fstream>
#include <cmath>
#include <exception>


// BOOST_PARAMETER_MAX_ARITY has to be included before mst_orient_normals
// if any CGAL::Mesh_3 stuff is to be included later without the compiler
// throwing a hissy fit, because Boost uses one arity as default and
// Mesh_3 defines another. We'll just include the file it's defined in
// to make it a little more future-proof.
#include <CGAL/Mesh_3/global_parameters.h>

#include <CGAL/grid_simplify_point_set.h>
#include <CGAL/jet_smooth_point_set.h>
#include <CGAL/jet_estimate_normals.h>
#include <CGAL/mst_orient_normals.h>
#include <CGAL/property_map.h>
#include <CGAL/compute_average_spacing.h>
#include <CGAL/Poisson_reconstruction_function.h>

#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Surface_mesh_default_criteria_3.h>
#include <CGAL/Surface_mesh_complex_2_in_triangulation_3.h>
#include <CGAL/Implicit_surface_3.h>

#include <CGAL/IO/output_surface_facets_to_polyhedron.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include "output_polyhedron_to_vtu.h"
#include "output_polyhedron_to_stl.h"
#include "output_image_3_to_stl.h"

#include <CGAL/Subdivision_method_3.h>

#include "polyhedron_utils.h"



template<typename Polyhedron, typename Image_3>
void make_smooth_polyhedron_from_discrete_image(
    Polyhedron& reconstruction_polyhedron, Image_3& image,
    double target_feature_size
) {
    typedef typename Polyhedron::Traits Kernel;

    typedef CGAL::Poisson_reconstruction_function<Kernel> Surface_reconstructor;

    typedef CGAL::Surface_mesh_default_triangulation_3 STr;
    typedef CGAL::Surface_mesh_default_criteria_3<STr> Surface_mesh_criteria;
    typedef CGAL::Surface_mesh_complex_2_in_triangulation_3<STr> C2t3;

    typedef
        CGAL::Implicit_surface_3<Kernel, Surface_reconstructor>
        Surface_mesh_domain;

    // Neighbour count for point set processing
    const int nb_neighbours = 18;


    // Some image data
    std::cout
        << std::endl
        << "Image Data:" << std::endl
        << "- Voxel Counts:" << std::endl
        << "-- xdim: " << image.xdim() << std::endl
        << "-- ydim: " << image.ydim() << std::endl
        << "-- zdim: " << image.zdim() << std::endl
        << "- Voxel Sizes:" << std::endl
        << "-- vx: " << image.vx() << std::endl
        << "-- vy: " << image.vy() << std::endl
        << "-- vz: " << image.vz() << std::endl
        << "- Image Sizes:" << std::endl
        << "-- xlen: " << image.xdim()*image.vx() << std::endl
        << "-- ylen: " << image.ydim()*image.vy() << std::endl
        << "-- zlen: " << image.zdim()*image.vz() << std::endl;

    // Find the volume by adding non-zero pixels
    double inr_volume = 0.0;
    for(std::size_t k=0; k<image.zdim(); k++)
    for(std::size_t j=0; j<image.ydim(); j++)
    for(std::size_t i=0; i<image.xdim(); i++) {
        if(image.value(i,j,k) != 0) {
            inr_volume += image.vx()*image.vy()*image.vz();
        }
    }
    std::cout
        << "-- volume: " << inr_volume << std::endl;


    // Find unique labels
    std::set<std::size_t> unique_labels;
    for(std::size_t k=0; k<image.zdim(); k++)
    for(std::size_t j=0; j<image.ydim(); j++)
    for(std::size_t i=0; i<image.xdim(); i++) {
        unique_labels.insert(image.value(i,j,k));
    }
    std::cout << "- Labels Found:";
    for(
        std::set<std::size_t>::iterator it = unique_labels.begin();
        it != unique_labels.end();
        it++
    ) {
        std::cout << " " << *it;
    }
    std::cout << std::endl;



    ///////////////////////////////////////////////////////////////////////////
    // Generate point set of surface points with oriented normals            //
    ///////////////////////////////////////////////////////////////////////////

    std::cout << std::endl << "Generating The Surface..." << std::endl;
    typedef CGAL::Point_with_normal_3<Kernel> PointWithNormal;
    std::vector<PointWithNormal> points;

    std::cout << "- Finding Edge Voxels..." << std::endl;

    // Push all edge points into the points list
    // (assuming single label)
    for(std::size_t k=0; k<image.zdim(); k++)
    for(std::size_t j=0; j<image.ydim(); j++)
    for(std::size_t i=0; i<image.xdim(); i++) {

        // Skip zero valued points
        if(image.value(i,j,k) == 0) continue;

        bool add_point = false;

        double x0 = i*image.vx() + 0.5*image.vx();
        double y0 = j*image.vy() + 0.5*image.vy();
        double z0 = k*image.vz() + 0.5*image.vz();

        double x = i*image.vx() + 0.5*image.vx();
        double y = j*image.vy() + 0.5*image.vy();
        double z = k*image.vz() + 0.5*image.vz();


        // Push sample point towards outside of particle
        if(i==0 || image.value(i-1,j,k)==0) {
            x -= 0.5*image.vx();
            add_point = true;
        }
        if(i==image.xdim()-1 || image.value(i+1,j,k)==0) {
            x += 0.5*image.vx();
            add_point = true;
        }
        if(j==0 || image.value(i,j-1,k)==0) {
            y -= 0.5*image.vy();
            add_point = true;
        }
        if(j==image.ydim()-1 || image.value(i,j+1,k)==0) {
            y += 0.5*image.vy();
            add_point = true;
        }
        if(k==0 || image.value(i,j,k-1)==0) {
            z -= 0.5*image.vz();
            add_point = true;
        }
        if(k==image.zdim()-1 || image.value(i,j,k+1)==0) {
            z += 0.5*image.vz();
            add_point = true;
        }

        if(add_point == true) {
            typename PointWithNormal::Point p(x,y,z);
            typename PointWithNormal::Point p0(x0,y0,z0);
            PointWithNormal pwn(p, p-p0);
            points.push_back(pwn);
        }
    }
    std::cout << "-- Surface Points Found: " << points.size() << std::endl;
    std::cout << "-- Done" << std::endl;


    // Simplify Surface Points
    // Seems to be needed sometimes...? If the Delaunay refinement
    // crashes with a Floating point exception, try using this.
    // TODO: Wire this up properly as an option?
    if(false) {
        std::cout << "- Simplifying Surface..." << std::endl;
        double grid_cell_size = 1.2*CGAL::min(
            image.vx(), CGAL::min(image.vy(), image.vz())
        );
        std::cout
            << "-- Finding points within " << grid_cell_size << " to remove..."
            << std::endl;
        typename std::vector<PointWithNormal>::iterator grid_points_begin =
            CGAL::grid_simplify_point_set(
                points.begin(), points.end(),
                grid_cell_size
            );
        std::cout << "--- Done" << std::endl;
        std::cout << "-- Removing..." << std::endl;
        points.erase(grid_points_begin, points.end());
        std::cout << "--- Done" << std::endl;
        std::cout << "-- Points left: " << points.size() << std::endl;
        std::cout << "-- Done" << std::endl;
    }

    //// Smooth point set
    //std::cout << "- Smoothing Surface Points..." << std::endl;
    //CGAL::jet_smooth_point_set(
    //    points.begin(), points.end(),
    //    CGAL::Identity_property_map<PointWithNormal>(),
    //    nb_neighbours
    //);


    // Estimate unoriented normals from list of points
    std::cout << "- Estimating Normals..." << std::endl;
    CGAL::jet_estimate_normals(
        points.begin(), points.end(),
        CGAL::Identity_property_map<PointWithNormal>(),
        CGAL::Normal_of_point_with_normal_pmap<Kernel>(),
        nb_neighbours
    );
    std::cout << "-- Done" << std::endl;


    // Orient the estimated normals
    std::cout << "- Orienting Normals..." << std::endl;
    typename std::vector<PointWithNormal>::iterator unoriented_points_begin =
        CGAL::mst_orient_normals(
            points.begin(), points.end(),
            CGAL::Identity_property_map<PointWithNormal>(),
            CGAL::Normal_of_point_with_normal_pmap<Kernel>(),
            nb_neighbours
        );
    std::cout << "-- Done" << std::endl;

    // Remove unoriented normals
    std::cout << "- Cleaning Unoriented Normals..." << std::endl;
    points.erase(unoriented_points_begin, points.end());

    std::cout << "-- Using " << points.size() << " Points." << std::endl;
    std::cout << "-- Done" << std::endl;



    ///////////////////////////////////////////////////////////////////////////
    // Reconstruct surface with list of surface points and normals           //
    ///////////////////////////////////////////////////////////////////////////

    std::cout << "- Constructing Implicit Function..." << std::endl;
    Surface_reconstructor poisson_reconstruction_function(
        points.begin(), points.end(),
        CGAL::Normal_of_point_with_normal_pmap<Kernel>()
    );

    // Do the poisson reconstruction and check it all worked properly
    if(!poisson_reconstruction_function.compute_implicit_function(true)) {
        std::cerr
            << "There was an error computing the surface function!"
            << std::endl;
        throw std::runtime_error(
            "Poisson reconstruction: compute_implicit_function() failed!"
        );
    }
    std::cout << "-- Done" << std::endl;

    // Finished generating the surface
    std::cout << "- Done" << std::endl;



    std::cout << std::endl << "Generating The Polyhedron..." << std::endl;

    ///////////////////////////////////////////////////////////////////////////
    // Generate coarse polyhedron from computed reconstruction function      //
    ///////////////////////////////////////////////////////////////////////////

    std::cout
        << "- Generating initial reconstruction polyhedron... " << std::endl;

    // Use average point spacing as the feature size.
    // This will be used as the basis of the meshing criteria.
    double reconstruction_feature_size = CGAL::compute_average_spacing(
        points.begin(), points.end(), nb_neighbours
    );

    double reconstruction_facet_distance = 0.3*reconstruction_feature_size;

    // Define the mesh domain from the poisson reconstruction function.
    Surface_mesh_domain poisson_reconstruction_mesh_domain(
        poisson_reconstruction_function,
        typename Kernel::Sphere_3(
            poisson_reconstruction_function.get_inner_point(),
            10*poisson_reconstruction_function
                .bounding_sphere().squared_radius()
        ),
        reconstruction_facet_distance/1000.
    );

    // Mesh criteria from the reconstruction_feature_size
    Surface_mesh_criteria poisson_reconstruction_mesh_criteria(
        25, 30*reconstruction_feature_size, reconstruction_facet_distance
    );

    // Make the mesh
    std::cout << "-- Meshing reconstruction domain..." << std::endl;
    STr poisson_reconstruction_tr;
    C2t3 poisson_reconstruction_c2t3(poisson_reconstruction_tr);
    CGAL::make_surface_mesh(
        poisson_reconstruction_c2t3,
        poisson_reconstruction_mesh_domain,
        poisson_reconstruction_mesh_criteria,
        CGAL::Manifold_tag()
    );
    std::cout << "--- Done" << std::endl;

    // Copy the mesh to a polyhedron for smoothing.
    std::cout << "-- Copying Mesh to Polyhedron..." << std::endl;
    CGAL::output_surface_facets_to_polyhedron(
        poisson_reconstruction_c2t3, reconstruction_polyhedron
    );
    std::cout << "--- Done" << std::endl;


    // Finished generating initial reconstruction polyhedron
    std::cout << "-- Done" << std::endl;


    // Run the smoothing until the feature size is within the requested
    // tolerance.
    std::cout << "- Smoothing Polyhedron..." << std::endl;
    std::cout
        << "-- Target feature size: " << target_feature_size << std::endl;

    while(true) {

        double current_feature_size = CGAL::compute_average_spacing(
            reconstruction_polyhedron.points_begin(),
            reconstruction_polyhedron.points_end(),
            nb_neighbours
        );

        std::cout
            << "-- Current feature size: " << current_feature_size
            << std::endl;

        if(current_feature_size <= target_feature_size) {
            std::cout << "-- Tolerance Reached. Continuing." << std::endl;
            break;
        }


        std::cout << "-- Smoothing 1 step..." << std::endl;

        // Choose the subdivision method
        //CGAL::Subdivision_method_3::Loop_subdivision(
        //    reconstruction_polyhedron, num_smoothing_steps
        //);
        //CGAL::Subdivision_method_3::DooSabin_subdivision(
        //    reconstruction_polyhedron, num_smoothing_steps
        //);
        //CGAL::Subdivision_method_3::CatmullClark_subdivision(
        //    reconstruction_polyhedron, num_smoothing_steps
        //);
        CGAL::Subdivision_method_3::Sqrt3_subdivision(
            reconstruction_polyhedron
        );
        std::cout << "--- Done" << std::endl;
    }

    std::cout << "-- Done" << std::endl;

    // Triangulate if necessary
    if(!reconstruction_polyhedron.is_pure_triangle()) {
        std::cout << "- Retriangulating Polyhedron..." << std::endl;
        CGAL::Polygon_mesh_processing::triangulate_faces(
            reconstruction_polyhedron
        );
        std::cout << "-- Done" << std::endl;
    }



    ///////////////////////////////////////////////////////////////////////////
    // Scale polyhedron as necessary, centered around the center of mass.    //
    ///////////////////////////////////////////////////////////////////////////

    // Match volumes
    std::cout << "- Matching volumes..." << std::endl;

    double reconstruction_polyhedron_volume = compute_polyhedron_volume(
        reconstruction_polyhedron
    );

    std::cout
        << "-- Polyhedron volume: "
            << reconstruction_polyhedron_volume
        << std::endl;

    std::cout << "-- INR volume: " << inr_volume << std::endl;

    // Scale factor is applied in each dimension. (V1/V2)^1/3 for 3D.
    double reconstruction_scale_factor =
        std::pow(inr_volume/reconstruction_polyhedron_volume, 1./3.);

    std::cout
        << "-- Rescaling by factor "
            << reconstruction_scale_factor
            << "..."
        << std::endl;

    typename Polyhedron::Traits::Aff_transformation_3
        reconstruction_affine_scaling(
            CGAL::SCALING,
            typename Polyhedron::Traits::RT(reconstruction_scale_factor)
        );
    for(
        typename Polyhedron::Point_iterator pit =
            reconstruction_polyhedron.points_begin();
        pit != reconstruction_polyhedron.points_end();
        pit++
    ) {
        *pit = pit->transform(reconstruction_affine_scaling);
    }
    std::cout << "--- Done" << std::endl;

    std::cout << "-- New polyhedron volume: "
        << compute_polyhedron_volume(reconstruction_polyhedron)
        << std::endl;

    // Finished matching volumes
    std::cout << "-- Done" << std::endl;


    // Match Centers of Mass
    std::cout << "- Matching Center Of Mass (COM)" << std::endl;

    // Find the polyhedron center of mass
    typename Polyhedron::Point_3 polyhedron_com =
        compute_polyhedron_center_of_mass(
            reconstruction_polyhedron
        );
    std::cout << "-- Polyhedron COM: " << polyhedron_com << std::endl;

    // Find the inr center of mass
    typename Polyhedron::Point_3 inr_com;
    std::size_t inr_count = 0;
    for(std::size_t k=0; k<image.zdim(); k++)
    for(std::size_t j=0; j<image.ydim(); j++)
    for(std::size_t i=0; i<image.xdim(); i++) {
        if(image.value(i,j,k) != 0) {
            // Add the center of the block
            inr_com = typename Polyhedron::Point_3(
                inr_com.x() + (i+0.5)*image.vx(),
                inr_com.y() + (j+0.5)*image.vy(),
                inr_com.z() + (k+0.5)*image.vz()
            );
            inr_count++;
        }
    }
    inr_com = typename Polyhedron::Point_3(
        inr_com.x()/inr_count, inr_com.y()/inr_count, inr_com.z()/inr_count
    );
    std::cout << "-- Inr COM: " << inr_com << std::endl;

    // Move polyhedron so centers of mass match
    std::cout
        << "-- Translating polyhedron by"
            << (CGAL::ORIGIN + (inr_com - polyhedron_com))
        << "..."
        << std::endl;
    typename Polyhedron::Traits::Aff_transformation_3
        reconstruction_affine_translation(
            CGAL::TRANSLATION, inr_com - polyhedron_com
        );
    for(
        typename Polyhedron::Point_iterator pit =
            reconstruction_polyhedron.points_begin();
        pit != reconstruction_polyhedron.points_end();
        pit++
    ) {
        *pit = pit->transform(
            reconstruction_affine_translation
        );
    }
    std::cout << "--- Done" << std::endl;

    polyhedron_com = compute_polyhedron_center_of_mass(
        reconstruction_polyhedron
    );
    std::cout << "-- New Polyhedron COM: " << polyhedron_com << std::endl;

    // Finished matching centers of mass
    std::cout << "-- Done" << std::endl;


    // Finished generating polyhedron
    std::cout << "- Done" << std::endl;

}


#endif //  MAKE_SMOOTH_POLYHEDRON_FROM_DISCRETE_IMAGE_H_
