#include <CGAL/Image_3.h>
#include "InrImageWriter.hpp"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <limits>
#include <vector>
#include <boost/cstdint.hpp>
#include <cstdlib>

int main(int argc, char* argv[]) {

    ///////////////////////////////////////////////////////////////////////////
    // Set commandline options                                               //
    ///////////////////////////////////////////////////////////////////////////

    std::string usage(
        "Usage: pluck_inr_labels input output label"
    );
    std::string description(
        "Extract the particle labelled LABEL from the input file and "
        " write it to the\noutput file with a tight bounding box."
    );


    // Setup the options //////////////////////////////////////////////////////

    po::positional_options_description positional_options;
    positional_options.add("input",  1);
    positional_options.add("output", 1);
    positional_options.add("label",  1);


    po::options_description required_options("Required");
    required_options.add_options() (
        "input,i", po::value<std::string>()->required(),
        "Input INR File."
    ) (
        "output,o", po::value<std::string>()->required(),
        "Output INR File."
    ) (
        "label,l", po::value<std::size_t>()->required(),
        "Label of particle to pluck."
    );

    
    po::options_description help_options("Help");
    help_options.add_options() (
        "help,h", "Print help message."
    );


    po::options_description cmdline_options;
    cmdline_options.add(required_options).add(help_options);


    // Parse the commandline arguments /////////////////////////////////////////

    po::variables_map args;

    try {
        po::store(
            po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .positional(positional_options)
                .run(),
            args
        );

        if(args.count("help")) {
            std::cout
                << std::endl
                << usage << std::endl << std::endl
                << description << std::endl
                << cmdline_options << std::endl;
            return 0;
        }

        po::notify(args);

    } catch (std::exception& e) {
        std::cout
            << std::endl
            << "Error: " << e.what() << std::endl << std::endl
            << usage << std::endl;
        return 1;
    }


    ///////////////////////////////////////////////////////////////////////////


    // Set values
    std::string input_filename  = args["input"].as<std::string>();
    std::string output_filename = args["output"].as<std::string>();
    std::size_t target_label    = args["label"].as<std::size_t>();


    //
    // Read the input file
    //
    std::cout << "Reading Inr File..." << std::endl;

    CGAL::Image_3 input_image;
    input_image.read(input_filename.c_str());

    // Original image statistics
    std::size_t input_xdim = input_image.xdim();
    std::size_t input_ydim = input_image.ydim();
    std::size_t input_zdim = input_image.zdim();

    double input_vx = input_image.vx();
    double input_vy = input_image.vy();
    double input_vz = input_image.vz();

    std::size_t input_vdim = input_image.image()->vdim;

    // Ensure vdim = 1
    if(input_vdim != 1) {
        std::cerr
            << "Expected input image vdim = 1. Instead got "
            << input_vdim
            << "!"
            << std::endl;

        return 1;
    }


    //
    // Find labelled maxs and mins
    //
    std::cout << "Finding labelled image dimensions..." << std::endl;


    // Target image statistics
    std::size_t output_xdim = 0;
    std::size_t output_ydim = 0;
    std::size_t output_zdim = 0;

    std::size_t output_xmax = std::numeric_limits<std::size_t>::min();
    std::size_t output_ymax = std::numeric_limits<std::size_t>::min();
    std::size_t output_zmax = std::numeric_limits<std::size_t>::min();

    std::size_t output_xmin = std::numeric_limits<std::size_t>::max();
    std::size_t output_ymin = std::numeric_limits<std::size_t>::max();
    std::size_t output_zmin = std::numeric_limits<std::size_t>::max();


    for(std::size_t k=0; k<input_zdim; k++)
    for(std::size_t j=0; j<input_ydim; j++)
    for(std::size_t i=0; i<input_xdim; i++) {
        std::size_t value = input_image.value(i,j,k);

        // Only check the expected label
        if(value != target_label) continue;

        // Set maxs
        if(output_xmax < i) output_xmax = i; 
        if(output_ymax < j) output_ymax = j; 
        if(output_zmax < k) output_zmax = k; 

        // Set mins
        if(output_xmin > i) output_xmin = i; 
        if(output_ymin > j) output_ymin = j; 
        if(output_zmin > k) output_zmin = k; 
    }

    output_xdim = output_xmax - output_xmin + 1;
    output_ydim = output_ymax - output_ymin + 1;
    output_zdim = output_zmax - output_zmin + 1;


    //
    // Copy iinput image data to output image data
    //
    std::cout << "Copying image data..." << std::endl;

    // Generate output image data
    std::vector<boost::uint16_t> output_image_data(
        output_xdim*output_ydim*output_zdim, 0
    );


    // Copy input image data to the output image data array
    for(std::size_t k=0; k<input_zdim; k++)
    for(std::size_t j=0; j<input_ydim; j++)
    for(std::size_t i=0; i<input_xdim; i++) {
        std::size_t value = input_image.value(i,j,k);

        // Only copy the expected label
        if(value != target_label) continue;

        // Find the relative positio in the output array
        std::size_t xpos = i - output_xmin;
        std::size_t ypos = j - output_ymin;
        std::size_t zpos = k - output_zmin;

        // Copy the data
        output_image_data.at(
            xpos + output_xdim*(ypos + output_ydim*zpos)
        ) = value;
    }


    //
    // Output the data to disk
    //
    std::cout << "Writing Image Data..." << std::endl;

    std::ofstream output_file(output_filename.c_str());
    
    InrImageWriter inr_image_writer(
        output_xdim, output_ydim, output_zdim,
        input_vx, input_vy, input_vz,
        input_vdim
    );

    inr_image_writer.write_as<boost::uint16_t>(
        output_image_data.begin(), output_image_data.end(), output_file
    );


    return 0;
}
