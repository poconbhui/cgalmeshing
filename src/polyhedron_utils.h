#ifndef POLYHEDRON_UTILS_H_
#define POLYHEDRON_UTILS_H_


#include <CGAL/Iterator_project.h>
#include <CGAL/barycenter.h>
#include <utility>


///////////////////////////////////////////////////////////////////////////////
// Compute polyhedron center of mass                                         //
///////////////////////////////////////////////////////////////////////////////
//
// We use a signed volume summation method to find the volume of the
// oriented polyhedron.
// We construct tetrahedra using the surface triangles from
// the polyhedron with a constant orientation and an arbitrary point.
// We then find the centroid of these tetrahedra, representing the center
// of mass of that tetrahedron and its signed volume.
// We use the centroid and the signed volume to find the
// barycenter (center of mass of a weighted average of points, ie the
// center of mass of the polyhedron)
// of the polyhedron.
//
// http://stackoverflow.com/questions/1838401/general-formula-to-calculate-polyhedron-volume
// http://en.wikipedia.org/wiki/Polyhedron#Volume
//

// Project facet iterator to (center of mass, signed volume) pair /////////////
template<typename Polyhedron>
struct ProjectCentroidVolumePair {

    // typedef the Polyhedron types here for convenience
    typedef typename Polyhedron::Traits Kernel;

    typedef typename Kernel::Point_3 Point_3;
    typedef typename Kernel::FT FT;


    // Typedef types needed by Iterator_project
    typedef typename Polyhedron::Facet argument_type;
    typedef std::pair<Point_3, FT> result_type;


    // Cache value so reference can be returned
    result_type cache_pair;


    // Compute the center of mass and signed volume from the
    // facet handle and store it in cache_pair
    void gen_cache_pair(const argument_type& facet_handle) {
        typename Polyhedron::Halfedge_around_facet_const_circulator hfc =
            facet_handle.facet_begin();

        Point_3 p1 = hfc->vertex()->point();
        hfc++;
        Point_3 p2 = hfc->vertex()->point();
        hfc++;
        Point_3 p3 = hfc->vertex()->point();

        typename Kernel::Tetrahedron_3 tet(
            CGAL::ORIGIN, p1, p2, p3
        );

        cache_pair.first = CGAL::centroid<Kernel>(tet);
        cache_pair.second = tet.volume();
    }


    const result_type& operator()(const argument_type& fit) {
        gen_cache_pair(fit);
        return cache_pair;
    }
};



// Compute the volume of a polyhedron /////////////////////////////////////////
template<typename Polyhedron>
typename Polyhedron::Traits::FT compute_polyhedron_volume(
    const Polyhedron& polyhedron
) {
    typedef
        CGAL::Iterator_project<
            typename Polyhedron::Facet_const_iterator,
            ProjectCentroidVolumePair<Polyhedron>
        >
        COMVol_iterator;

    if(!polyhedron.is_pure_triangle()) {
        throw std::logic_error(
            "compute_polyhedron_center_of_mass: "
            "Input polyhedron must be pure triangular."
        );
    }

    typedef typename Polyhedron::Traits::FT FT;

    FT volume = FT(0.0);
    for(
        COMVol_iterator comvol_it = polyhedron.facets_begin();
        comvol_it != polyhedron.facets_end();
        comvol_it++
    ) {
        volume += comvol_it->second;
    }

    return volume;
}


// Compute the center of mass for a given triangulated polyhedron /////////////
template<typename Polyhedron>
typename Polyhedron::Point_3 compute_polyhedron_center_of_mass(
    const Polyhedron& polyhedron
) {

    typedef
        CGAL::Iterator_project<
            typename Polyhedron::Facet_const_iterator,
            ProjectCentroidVolumePair<Polyhedron>
        >
        COMVol_iterator;

    if(!polyhedron.is_pure_triangle()) {
        throw std::logic_error(
            "compute_polyhedron_center_of_mass: "
            "Input polyhedron must be pure triangular."
        );
    }

    return CGAL::barycenter< COMVol_iterator >(
        polyhedron.facets_begin(), polyhedron.facets_end()
    );
}


#endif // POLYHEDRON_UTILS_H_
