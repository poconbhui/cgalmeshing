#include <CGAL/Image_3.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <cstdlib>
#include <map>
#include <limits>
#include <vector>
#include <boost/cstdint.hpp>

#include <time.h>

#include "InrImageWriter.hpp"


std::string size_string(std::size_t ibytes) {
    char buf[20];
    double bytes = ibytes;

    if(bytes < 1024) {
       std::sprintf(buf, "%.4f  B", bytes);
       return buf;
    }

    bytes /= 1024;
    if(bytes < 1024) {
        std::sprintf(buf, "%.4f KB", bytes);
        return buf;
    }

    bytes /= 1024;
    if(bytes < 1024) {
        std::sprintf(buf, "%.4f MB", bytes);
        return buf;
    }

    bytes /= 1024;
    if(bytes < 1024) {
        std::sprintf(buf, "%.4f GB", bytes);
        return buf;
    }

    bytes /= 1024;
    if(bytes < 1024) {
        std::sprintf(buf, "%.4f TB", bytes);
        return buf;
    }

    return "> 1 PB";
}




int main(int argc, char* argv[]) {

    ///////////////////////////////////////////////////////////////////////////
    // Set commandline options                                               //
    ///////////////////////////////////////////////////////////////////////////

    std::string usage(
        "Usage: percolator input output"
    );
    std::string description(
        "Uniquely labels contiguous space in an INR file."
    );


    // Setup the options //////////////////////////////////////////////////////

    po::positional_options_description positional_options;
    positional_options.add("input",  1);
    positional_options.add("output", 1);


    po::options_description required_options("Required");
    required_options.add_options() (
        "input,i", po::value<std::string>()->required(),
        "The input INR file to label."
    ) (
        "output,o", po::value<std::string>()->required(),
        "The labelled INR file name."
    );


    po::options_description help_options("Help");
    help_options.add_options() (
        "help,h", "Print this help message."
    );


    po::options_description cmdline_options;
    cmdline_options.add(required_options).add(help_options);


    // Parse the commandline arguments /////////////////////////////////////////

    po::variables_map args;

    try {
        po::store(
            po::command_line_parser(argc, argv)
                .options(cmdline_options)
                .positional(positional_options)
                .run(),
            args
        );

        if(args.count("help")) {
            std::cout
                << std::endl
                << usage << std::endl << std::endl
                << description << std::endl
                << cmdline_options << std::endl;
            return 0;
        }

        po::notify(args);

    } catch(std::exception& e) {
        std::cout
            << std::endl
            << "Error: " << e.what() << std::endl << std::endl
            << usage << std::endl;
        return 1;
    }


    ///////////////////////////////////////////////////////////////////////////


    // Set input filename and check existance.
    std::string input_filename = args["input"].as<std::string>();
    {
        std::ifstream inf(input_filename.c_str());
        if(!inf.good()) {
            std::cerr
                << "Unable to open " << input_filename << "!"
                << std::endl;
        }
    }

    // Open output file and check goodness
    std::string output_filename = args["output"].as<std::string>();
    std::ofstream output_file_stream(output_filename.c_str());
    if(!output_file_stream.good()) {
        std::cerr
            << "Unable to open " << output_filename << "!"
            << std::endl;
    }


    size_t space_id = 0;


    ///////////////////////////////////////////////////////////////////////////
    // Read the input image.                                                 //
    ///////////////////////////////////////////////////////////////////////////
    std::cout << std::endl << "Reading Image..." << std::endl;
    CGAL::Image_3 image;
    image.read(input_filename.c_str());

    // Some useful values.
    //
    // I'm just being funny here, but I'm not entirely certain what
    // the maximum representation the types for image.xdim() etc have.
    // std::size_t is guaranteed to be able to uniquely label everything
    // in memory.
    std::size_t xdim = image.xdim();
    std::size_t ydim = image.ydim();
    std::size_t zdim = image.zdim();
    std::size_t image_size = xdim*ydim*zdim;

    std::cout << "-Voxel Counts:" << std::endl
        << "--x:     " << xdim << std::endl
        << "--y:     " << ydim << std::endl
        << "--z:     " << zdim << std::endl
        << "--total: " << image_size << std::endl;


    ///////////////////////////////////////////////////////////////////////////
    // Copy the image data to a larger data format and uniquely label        //
    // each voxel.                                                           //
    ///////////////////////////////////////////////////////////////////////////
    //
    // Create copy of the image data to a std::size_t array so
    // the datatype is large enough to uniquely label each voxel.
    //
    // Also use a vector for some convenience (debugging etc).
    //
    std::cout << std::endl << "Converting Data..." << std::endl;


    // Using std::size_t is probably, once again, overkill,
    // but it's only 8x larger than an 8 bit array.
    //
    // Quick testing on the file this was written for suggests
    // at least 32 bit type is needed.
    typedef std::vector<std::size_t> ImageData;
    ImageData image_data(image_size, 0);

    std::cout
        << "-Storing in array of size: "
        << size_string(image_data.capacity()*sizeof(std::size_t))
        << std::endl;
    

    // Give each labellable voxel a unique label
    std::size_t new_label = 1;
    for(std::size_t k=0; k<zdim; k++)
    for(std::size_t j=0; j<ydim; j++)
    for(std::size_t i=0; i<xdim; i++) {

        if(image.value(i,j,k) == space_id) {

            // Set space label
            image_data[i+xdim*(j+ydim*k)] = 0;

        } else {

            // Set non-space label
            image_data[i+xdim*(j+ydim*k)] = new_label;
            new_label++;

        }
    }
    new_label--; // Undo final unused new_label++
    std::size_t label_max = new_label;

    std::cout << "-Label Max: " << label_max << std::endl;


    ///////////////////////////////////////////////////////////////////////////
    // Do percolation                                                        //
    ///////////////////////////////////////////////////////////////////////////
    std::cout << std::endl << "Running Percolation..." << std::endl;

    clock_t startTime_percolation = clock();

    std::size_t percolation_iteration = 0;
    std::size_t num_percolations      = 0;

    // Run loop until no percolations happen.
    do {
        num_percolations = 0;

        // Define the percolation loop body here to avoid
        // writing it twice for forward and reverse loops
        class PercolationLoopBody {
        public:
            bool operator()(
                std::size_t i, std::size_t j, std::size_t k,
                std::size_t xdim, std::size_t ydim, std::size_t zdim,
                ImageData& image_data
            ) {
                bool is_percolated = false;
                std::size_t& value = image_data[i+xdim*(j+ydim*k)];

                // Don't percolate 0's.
                if(value == 0) return is_percolated;

                // Check neighbouring elements (including diagonals).
                // Set self to largest neighbour element.
                for(int kn=-1; kn <=1; kn++)
                for(int jn=-1; jn <=1; jn++)
                for(int in=-1; in <=1; in++) {

                    // Use std::abs so when, say, i=0, in=-1, we get
                    // i+in = 1 instead of -1, and 1 should be an
                    // index for one of the neighbours anyway.
                    std::size_t neighbour_i = std::abs(static_cast<int>(i)+in);
                    std::size_t neighbour_j = std::abs(static_cast<int>(j)+jn);
                    std::size_t neighbour_k = std::abs(static_cast<int>(k)+kn);

                    // Skip out of bounds indices
                    // Using unsigned index means index < 0 never happens,
                    // instead it should just be a HUGE number.
                    // Definitely (probably) larger than the image dimensions.
                    if(
                           neighbour_i >= xdim
                        || neighbour_j >= ydim
                        || neighbour_k >= zdim
                    ) continue;


                    std::size_t neighbour_value = image_data[
                            neighbour_i+xdim*(neighbour_j+ydim*neighbour_k)
                    ];

                    if(value < neighbour_value) {
                        value = neighbour_value;
                        is_percolated=true;
                    }
                }

                return is_percolated;
            }
        };
        PercolationLoopBody percolation_loop_body;


        // Loop forward over each voxel
        for(std::size_t k=0; k<zdim; k++)
        for(std::size_t j=0; j<ydim; j++)
        for(std::size_t i=0; i<xdim; i++) {
            if( percolation_loop_body(i,j,k, xdim,ydim,zdim, image_data) ) {
                num_percolations++;
            }
        }

        // And backwards
        for(std::size_t k=zdim-1; k<zdim; k--)
        for(std::size_t j=ydim-1; j<ydim; j--)
        for(std::size_t i=xdim-1; i<xdim; i--) {
            if( percolation_loop_body(i,j,k, xdim,ydim,zdim, image_data) ) {
                num_percolations++;
            }
        }


        // Output percolation progress
        percolation_iteration++;
        std::cout
            << "-Iteration: " << percolation_iteration
            << ", "
            << "Percolations: " << num_percolations << std::endl;

    } while(num_percolations > 0);

    std::cout
        << "-Percolation Time: "
        << double( clock() - startTime_percolation ) / (double)CLOCKS_PER_SEC
        << " seconds."
        << std::endl;


    ///////////////////////////////////////////////////////////////////////////
    // Relabel the clusters from 1 to num_clusters                           //
    ///////////////////////////////////////////////////////////////////////////
    std::cout << std::endl << "Relabelling Clusters..." << std::endl;


    // Relabel from 1 to num_clusters with the lowest label
    // at the lowest pixel.
    std::map<std::size_t, std::size_t> relabel_map;
    std::size_t max_new_id = 1; // Start at 1 because 0 is still space!

    clock_t startTime_relabelling = clock();
    for(
        ImageData::iterator id_it = image_data.begin();
        id_it != image_data.end();
        id_it++
    ) {
        // Don't relabel space.
        if(*id_it == 0) continue;

        std::size_t& new_id = relabel_map[*id_it];

        // If relabel id not set, set it
        if(new_id == 0) {
            new_id = max_new_id;
            max_new_id++;
        }

        *id_it = new_id;
    }
    max_new_id--; // Undo final unused max_new_id++
    std::size_t max_label = max_new_id;

    std::cout
        << "-Relabelling Time: "
        << double( clock() - startTime_relabelling ) / (double)CLOCKS_PER_SEC
        << " seconds."
        << std::endl;

    std::cout << "-Label Stats:" << std::endl;
    std::cout << "--Min Label:     1" << std::endl;
    std::cout << "--Max Label:     "  << max_label << std::endl;
    std::cout << "--Unique Labels: "  << relabel_map.size() << std::endl;


    ///////////////////////////////////////////////////////////////////////////
    // Output labelled image to disk                                         //
    ///////////////////////////////////////////////////////////////////////////
    std::cout << std::endl << "Outputting labelled image..." << std::endl;


    InrImageWriter output_image(
        xdim, ydim, zdim,
        image.vx(), image.vy(), image.vz(),
        1
    );

    // Write data to disk with lowest usable bit representation
    if(max_label < std::numeric_limits<boost::uint8_t>::max()) {
        output_image.write_as<boost::uint8_t>(
            image_data.begin(), image_data.end(), output_file_stream
        );
    } else if (max_label < std::numeric_limits<boost::uint16_t>::max()) {
        output_image.write_as<boost::uint16_t>(
            image_data.begin(), image_data.end(), output_file_stream
        );
    } else if (max_label < std::numeric_limits<boost::uint32_t>::max()) {
        output_image.write_as<boost::uint32_t>(
            image_data.begin(), image_data.end(), output_file_stream
        );
    } else {
        output_image.write_as<boost::uint64_t>(
            image_data.begin(), image_data.end(), output_file_stream
        );
    }



    return 0;
}
