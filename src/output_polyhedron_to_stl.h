#ifndef OUTPUT_PARTICLE_TO_STL_H_
#define OUTPUT_PARTICLE_TO_STL_H_


#include <exception>

template<typename Stream, typename Polyhedron>
void output_polyhedron_to_stl(
    Stream& polyhedron_stl, const Polyhedron& polyhedron
) {
    typedef typename Polyhedron::Facet_const_iterator PFit;
    typedef
        typename Polyhedron::Halfedge_around_facet_const_circulator
        PHc;
    typedef typename Polyhedron::Point_3 Point_3;

    // The polyhedron must be triangular!
    if(!polyhedron.is_pure_triangle()) {
        throw std::logic_error(
            "output_polyhedron_stl: Input polyhedron must be pure triangular."
        );
    }

    polyhedron_stl << "solid particle\n";
    for(
        PFit pfit = polyhedron.facets_begin();
        pfit != polyhedron.facets_end();
        pfit++
    ) {
        PHc phc = pfit->facet_begin();
        PHc phc_end = phc;

        polyhedron_stl
            << "facet normal 0 0 0\n"
            << "   outer loop\n";
        do {
            const Point_3& p = phc->vertex()->point();

            polyhedron_stl
                << "        vertex "
                << p.x() << " " << p.y() << " " << p.z() << "\n";

            phc++;
        } while(phc != phc_end);
        polyhedron_stl
            << "   endloop\n"
            << "endfacet\n";
    }
    polyhedron_stl << "endsolid particle" << "\n";
}


#endif // OUTPUT_PARTICLE_TO_STL_H_
