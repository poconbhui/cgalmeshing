#!/usr/bin/env bash

# Do tiff to inr conversion and labelling?
#generate_labelled_inr=false
generate_labelled_inr=true

if [[ $# -ne 3 ]]; then
    echo "Usage: ./demo.sh tiff_filename output_directory particle_name"
    exit 1
fi

tiff_filename="$1"
output_directory="$2"
particle_name="$3"


# Remove trailing slash from output_directory if present
output_directory="$(echo "$output_directory" | sed 's#/*$##')"


#
# Set directories and stack files
#

data_filename_base="$(basename tiff_filename)"
# Voxel size of the input stack
data_vox=( 0.0097656 0.0132033 0.020 )

#output_directory=tmp_data
mkdir -p $output_directory


#
# The particle to mesh
#
particle_cell_size=0.005


case $particle_name in
    "average_prolate")
        # Average Prolate
        particle_name="average_prolate"
        particle_coords=( 7.28179 5.41218 2.08234 )
        ;;

    "average_oblate")
        # Average Oblate
        particle_name="average_oblate"
        particle_coords=( 7.26379 3.65038 3.82084 )
        ;;

    "small_prolate")
        # Small Prolate
        particle_name="small_prolate"
        particle_coords=( 2.53708 6.41858 0.549919 )
        ;;

    "small_oblate")
        # Small Oblate
        particle_name="small_oblate"
        particle_coords=( 1.57715 12.3824 3.1475 )
        ;;

    *)
        echo "Unknown particle \"$particle_name\""
        exit 1
esac



if $generate_labelled_inr
then
    # Convert TIFF to INR
    echo
    echo ---
    echo --- Running tiff2inr
    echo ---
    echo
    # This uses imagemagick and some gross hacks to convert a tiff file to
    # an inr file, which is used by CGAL. It may have been possible to use
    # the tiff directly with CGAL, but everything has now been written with
    # inr in mind.
    tiff2inr \
        "$tiff_filename" \
        "$output_directory/${data_filename_base}.unlabelled.inr" \
        ${data_vox[0]} ${data_vox[1]} ${data_vox[2]}


    # Label individual particles
    echo
    echo ---
    echo --- Labelling individual particles
    echo ---
    echo
    # This uses a percolation algorithm to give unique labels to contiguous
    # regions in the inr file. This is used for uniquely identifying
    # particles in the input file.
    percolator \
        "$output_directory/${data_filename_base}.unlabelled.inr" \
        "$output_directory/${data_filename_base}.labelled.inr"
fi


# Find label for average prolate
echo
echo ---
echo --- Finding label for average prolate particle
echo ---
echo

# This program looks at all the labelled particles in the inr file and
# outputs some data on them. It can also mark a particle where the point
# terget falls within the particle by a simple test of whether that point
# falls within that particle's bounding box. This also tends to return
# the label of the surrounding medium, since that has a bounding box of
# the entire file.

# Find data for all particles
stats=$(
    particle_stats \
        "$output_directory/${data_filename_base}.labelled.inr" \
        --target_xyz \
            ${particle_coords[0]} \
            ${particle_coords[1]} \
            ${particle_coords[2]}
)
echo $stats
# We strip the digits from the label entry
label=$(
    echo "$stats" \
        | grep Label \
        | sed 's/^Label \([[:digit:]]\+\).*$/\1/'
)

# And output our results
echo
echo --- Found Particle:
echo
echo "$target_stats"


# Make a standalone file for the desired particle
echo
echo ---
echo --- Creating standalone particle file
echo ---
echo
# This generates an inr file containing only the labelled particle.
# It also trims the file down to a tight bounding box for the particle, so
# some positional information is lost in the process.
pluck_inr_labels \
    "$output_directory/${data_filename_base}.labelled.inr" \
    "$output_directory/${particle_name}.inr" \
    $label


# Create Mesh
echo
echo ---
echo --- Creating Mesh
echo ---
echo
# This takes an inr file, creates a smooth approximation of the data
# from the blocky horror it's given, adds two surrounding spheres, 
# meshes it, and outputs it in a dolfin xml format for use with the FEniCS
# environment.
#
# It should soon run with optional sphere adding and also output to a
# pattran format.
meshinr \
    "$output_directory/${particle_name}.inr" \
    "$output_directory/${particle_name}" \
    $particle_cell_size \
    -m stl -V no
