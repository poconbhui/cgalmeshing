This is a set of programs to process and mesh particles from
a stack of fib images.

It's mostly exploratory work and hacks to get a particular stack meshed
, so it's not really intended for general use.

For an idea of a useable workflow, look at demo.sh


The project requirements are:

- CMake
- ImageMagick
- Boost
- CGAL
