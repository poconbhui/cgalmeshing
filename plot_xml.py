#!/usr/bin/env python

import dolfin
import math
import sys

options = {
    "mesh_sizes": True,
    "mesh_bounds": False,
    "mesh_spacing": False,
    "plot_meshes": False
}
# Update options using command line options
# in the form
#   python plot_xml.py mesh_file mesh_spacing=True plot_meshes=False
mesh_filename = sys.argv[1]
for arg in sys.argv[2:]:
    try:
        eval("options.update("+arg+")")
    except:
        print "Invalid command line option: ", arg
        sys.exit(1)

print "-- Loading Mesh"
mesh = dolfin.Mesh(mesh_filename)
mesh_fn = dolfin.MeshFunction('size_t', mesh, mesh_filename)
print "--- Done"

#mesh_fn_cpy = dolfin.MeshFunction('size_t', mesh_fn)
#for cell in dolfin.cells(mesh):
#    if mesh_fn_cpy[cell] != 1: mesh_fn_cpy[cell] = 2
#particle_mesh = dolfin.SubMesh(mesh, mesh_fn_cpy, 1)
#space_mesh = dolfin.SubMesh(mesh, mesh_fn_cpy, 2)
##dolfin.plot(particle_mesh, interactive=True)
##dolfin.plot(space_mesh, interactive=True)



if options['mesh_sizes']:
    # Mesh sizes
    print
    print "Num Vertices: ", mesh.num_vertices()
    print "Num Cells: ", mesh.num_cells()

if options['mesh_bounds']:
    # Mesh bounds
    xmin = float('inf')
    ymin = float('inf')
    zmin = float('inf')

    xmax = float('-inf')
    ymax = float('-inf')
    zmax = float('-inf')

    # Particle bounds
    for vertex in dolfin.vertices(particle_mesh):
        p = vertex.point()

        if xmin > p.x(): xmin = p.x()
        if ymin > p.y(): ymin = p.y()
        if zmin > p.z(): zmin = p.z()

        if xmax < p.x(): xmax = p.x()
        if ymax < p.y(): ymax = p.y()
        if zmax < p.z(): zmax = p.z()

    print
    print "Particle Coord Min: ", (xmin, ymin, zmin)
    print "Particle Coord Max: ", (xmax, ymax, zmax)


    for vertex in dolfin.vertices(space_mesh):
        p = vertex.point()

        if xmin > p.x(): xmin = p.x()
        if ymin > p.y(): ymin = p.y()
        if zmin > p.z(): zmin = p.z()

        if xmax < p.x(): xmax = p.x()
        if ymax < p.y(): ymax = p.y()
        if zmax < p.z(): zmax = p.z()

    print
    print "Total Coord Min: ", (xmin, ymin, zmin)
    print "Total Coord Max: ", (xmax, ymax, zmax)


if options['mesh_spacing']:
    # Neighbour spacing
    d_tot = 0
    d_count = 0
    d_max = float('-inf')
    d_min = float('inf')

    for edge in dolfin.edges(particle_mesh):
        d = edge.length()
        d_tot += d
        d_count += 1

        # Find min/max
        if d_min > d: d_min = d
        if d_max < d: d_max = d

    d_avg = d_tot/d_count
    print
    print "Particle Average Neighbour distance: ", d_avg
    print "Particle Max Neighbour distance: ", d_max
    print "Particle Min Neighbour distance: ", d_min


    for edge in dolfin.edges(mesh):
        d = edge.length()
        d_tot += d
        d_count += 1

        # Find min/max
        if d_min > d: d_min = d
        if d_max < d: d_max = d

    d_avg = d_tot/d_count
    print
    print "Total Average Neighbour distance: ", d_avg
    print "Total Max Neighbour distance: ", d_max
    print "Total Min Neighbour distance: ", d_min


if options['plot_meshes']:
    print
    print "Plotting..."

    dolfin.plot(mesh, title="Mesh")
    dolfin.plot(mesh_fn, title="Mesh Function")

    dolfin.interactive()


print "outputting"
dolfin.File("out.mesh.pvd") << mesh
dolfin.File("out.mesh_fn.pvd") << mesh_fn
